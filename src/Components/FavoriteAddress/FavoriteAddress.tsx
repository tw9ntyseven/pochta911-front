import React, { useState } from 'react'
import { UserState } from '../../Screens/CreateClient/CreateClient';
import { Alert } from '../Alert/Alert';
import "./FavoriteAddress.css";

interface IFavoriteAddress extends UserState {
    favoriteAddressesAdd: any,
    favoriteAddressesDel: any,
    save: any,
}

export const FavoriteAddress : React.FC<IFavoriteAddress> = ({userData, favoriteAddressesAdd, favoriteAddressesDel, save }) => {
    const [address, setAddress] = useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [notes, setNotes] = useState('');
    const [identeficator, setIdenteficator] = useState('');

    const [showMessage, setShowMessage] = useState(false);
    const [message, setMessage] = useState('');

    
    const AddAddress = () => {
        if (address == "" && name == "" && phone == "" && notes == "") {
            setShowMessage(!showMessage);
            setMessage("Заполните все поля!");
        } else {
        favoriteAddressesAdd.push({
            address,
            name,
            phone,
            notes,
        })
        // setMessage("Добавлено, для сохранения нажмите СОХРАНИТЬ");
        // setShowMessage(!showMessage);
        save();
    }
    }
    console.log(save, "Save");
    
    const DelAddress = (id: string) => {
        setIdenteficator(id);
        favoriteAddressesDel.push({
            id
        })
        // setMessage("Удалено, для сохранения нажмите СОХРАНИТЬ");
        // setShowMessage(!showMessage);
        save();
    }


    return (
        <>
        {showMessage ?
        <Alert message={message} />
        :null}
        <div style={{marginBottom: '15px'}} className="favorite-address">
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Адрес:</div>
            <input onChange={e => setAddress(e.target.value)} defaultValue={address} type="text" className="ipt btn__right create-cour__ipt"/>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Имя:</div>
            <input onChange={e => setName(e.target.value)} defaultValue={name} type="text" className="ipt btn__right create-cour__ipt"/>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Телефон:</div>
            <input onChange={e => setPhone(e.target.value)} defaultValue={phone} type="text" className="ipt btn__right create-cour__ipt"/>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Примечания:</div>
            <input onChange={e => setNotes(e.target.value)} defaultValue={notes} type="text" className="ipt btn__right create-cour__ipt"/>
        </div>
        <button onClick={e => AddAddress()} className="btn btn__right">Добавить</button>
        </div>
        { userData.data.favoriteAddresses.length !== 0 ? userData.data.favoriteAddresses.map(({id, address, name, notes, phone}:any) => (
        <div key={id} className="favorite-address favorite-address__item">
        <div onClick={e => DelAddress(id)} className="favorite-address__close"><i className="fas fa-times"></i></div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Адрес:</div>
            <div className="btn__right bold favorite-address__text">{address}</div>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Имя:</div>
            <div className="btn__right bold favorite-address__text">{name}</div>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Телефон:</div>
            <div className="btn__right bold favorite-address__text">{phone}</div>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="home__title">Примечания:</div>
            <div className="btn__right bold favorite-address__text">{notes}</div>
        </div>
        </div>
        )): "Нет любимых адресов"}
        {/* <div style={{width: '100px',marginTop: '15px'}} className="flex btn__right">
            <button onClick={e => AddAddress()} className="btn auto__btn create-order__btn"><i className="fas fa-plus"></i></button>
            <button style={{background: "#d9534f"}} className="btn auto__btn create-order__btn"><i className="fas fa-minus"></i></button>
        </div> */}
        </>
    );
}