import React from 'react'
import { Link } from 'react-router-dom';

type ITableCliMapProps = {
    items: Array<any>,
}

const TableCliMap = ({items}: ITableCliMapProps) => {
    return (
    <tbody>
        {items.map((item, index) => (
            <tr key={index} className="table-map">
                <td className="table-map__item">{item.id}</td>
                <td className="table-map__item">{item.userName}</td>
                <td className="table-map__item">{item.company}</td>
                <td className="table-map__item">{item.login}</td>
                <td className="table-map__item">{item.orders}</td>
                <td className="table-map__item">{item.phone}</td>
                <td className="table-map__item">{item.telegramId}</td>
                <td className="table-map__item">{item.viberId}</td>
                <td className="table-map__item">{item.email}</td>
                <td className="table-map__item">{item.dateReg}</td>
                <td className="table-map__item"><Link style={{color: "#4cae4c"}} to="/clients/client"><i className="fas fa-edit"></i></Link></td>
                <td className="table-map__item"><i className="fas fa-times-circle"></i></td>
            </tr>
        ))}
    </tbody>
    );
}

export const TableCli : React.FC = () => {
    return (
        <table className="table-auto">
            <thead>
                <tr className="table__title-overlay">
                    <th className="table__title">Id</th>
                    <th className="table__title">Контактное лицо</th>
                    <th className="table__title">Компания</th>
                    <th className="table__title">Логин</th>
                    <th className="table__title">Заказов</th>
                    <th className="table__title">Телефон</th>
                    <th className="table__title">Telegram Id</th>
                    <th className="table__title">Viber Id</th>
                    <th className="table__title">Почта</th>
                    <th className="table__title">Дата регистрации</th>
                    <th className="table__title"></th>
                    <th className="table__title"></th>
                </tr>
            </thead>
            <TableCliMap items={[
                {
                    id: "83",
                    userName: "Иванов Иван",
                    company: "Рога и Копыта",
                    login: "rik",
                    orders: "31",
                    phone: "9788888888",
                    telegramId: "0",
                    viberId: "0",
                    email: "pochta@mail.ru",
                    dateReg: "14.12.2014"
                },
                {
                    id: "83",
                    userName: "Иванов Иван",
                    company: "Рога и Копыта",
                    login: "rik",
                    orders: "31",
                    phone: "9788888888",
                    telegramId: "0",
                    viberId: "0",
                    email: "pochta@mail.ru",
                    dateReg: "14.12.2014"
                },                {
                    id: "83",
                    userName: "Иванов Иван",
                    company: "Рога и Копыта",
                    login: "rik",
                    orders: "31",
                    phone: "9788888888",
                    telegramId: "0",
                    viberId: "0",
                    email: "pochta@mail.ru",
                    dateReg: "14.12.2014"
                },
            ]} />
        </table>
    );
}
