import React from 'react'
import { Link } from 'react-router-dom';

type ITableAutoMapProps = {
    items: Array<any>,
}

const TableAutoMap = ({items}: ITableAutoMapProps) => {
    return (
    <tbody>
        {items.map((item, index) => (
            <tr key={index} className="table-map">
                <td className="table-map__item">{item.numberId}</td>
                <td className="table-map__item">{item.userName}</td>
                <td className="table-map__item">{item.phone}</td>
                <td className="table-map__item">{item.viber}</td>
                <td className="table-map__item">{item.email}</td>
                <td className="table-map__item">{item.typeOfAuto}</td>
                <td className="table-map__item">{item.brand}</td>
                <td className="table-map__item">{item.year}</td>
                <td className="table-map__item">{item.number}</td>
                <td className="table-map__item">{item.capacity}</td>
                <td className="table-map__item"><Link style={{color: "#4cae4c"}} to="/clients/client"><i className="fas fa-edit"></i></Link></td>
                <td className="table-map__item"><i className="fas fa-times-circle"></i></td>
            </tr>
        ))}
    </tbody>
    );
}

export const TableAuto : React.FC = () => {
    return (
        <table className="table-auto">
            <thead>
                <tr className="table__title-overlay">
                    <th className="table__title">№ [id]</th>
                    <th className="table__title">ФИО</th>
                    <th className="table__title">Телефон</th>
                    <th className="table__title">Viber</th>
                    <th className="table__title">Email</th>
                    <th className="table__title">Тип машины</th>
                    <th className="table__title">Марка</th>
                    <th className="table__title">Год</th>
                    <th className="table__title">Номер</th>
                    <th className="table__title">Объем</th>
                    <th className="table__title"></th>
                    <th className="table__title"></th>
                </tr>
            </thead>
            <TableAutoMap items={[
                {
                    numberId: "1 [344]",
                    userName: "Иванов Иван Иванович",
                    phone: "+79787777777",
                    viber: "qEqnab80jNv8HVQSEjMexQ==",
                    email: "you@mail.ru",
                    typeOfAuto: "седан",
                    brand: "Ваз 2113",
                    year: "2006",
                    number: "С636ТМ",
                    capacity: "1"
                },
                {
                    numberId: "1 [344]",
                    userName: "Иванов Иван Иванович",
                    phone: "+79787777777",
                    viber: "qEqnab80jNv8HVQSEjMexQ==",
                    email: "you@mail.ru",
                    typeOfAuto: "седан",
                    brand: "Ваз 2113",
                    year: "2006",
                    number: "С636ТМ",
                    capacity: "1"
                },
                {
                    numberId: "1 [344]",
                    userName: "Иванов Иван Иванович",
                    phone: "+79787777777",
                    viber: "qEqnab80jNv8HVQSEjMexQ==",
                    email: "you@mail.ru",
                    typeOfAuto: "седан",
                    brand: "Ваз 2113",
                    year: "2006",
                    number: "С636ТМ",
                    capacity: "1"
                },
            ]} />
        </table>
    );
}