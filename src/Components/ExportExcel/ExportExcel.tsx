import React, { useState } from 'react'
import './ExportExcel.css'
import DatePicker, { registerLocale } from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import ru from 'date-fns/locale/ru';
registerLocale('ru', ru)


export interface IExportExcel {
    onClick: React.MouseEventHandler<HTMLElement>,
}

export const ExportExcel : React.FC<IExportExcel> = ({onClick}) => {
    const [ dateValue, setDateValue ] = useState<any>(null);
    const [ dateToValue, setDateToValue ] = useState<any>(null);

    return (
        <div className="export-excel__layer">
            <div className="export-excel">
                <div className="flex export-excel__title-block">
                    <div className="home__title">Экспорт в эксель</div>
                    <div className="export-excel__btn" onClick={onClick}><i className="fas fa-times"></i></div>
                </div>
                <div className="flex export-excel__inputs">
                   <DatePicker
                    locale="ru"
                    placeholderText="01.11.2020"
                    className="ipt home__ipt-datepicker export-excel__datepicker"
                    selected={dateValue}
                    onChange={date => setDateValue(date)}
                    dateFormat='dd.MM.yyyy'
                    />
                    <div className="home__title">до</div>
                    <DatePicker
                    locale="ru"
                    placeholderText="01.11.2020"
                    className="ipt home__ipt-datepicker"
                    selected={dateToValue}
                    onChange={date => setDateToValue(date)}
                    dateFormat='dd.MM.yyyy'
                    />
                    <button className="btn">excel</button>
                </div>
            </div>
        </div>
    );
}