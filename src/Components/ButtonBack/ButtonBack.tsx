import { createBrowserHistory } from 'history';
import React from 'react'
import './ButtonBack.css'
export const history = createBrowserHistory();


export const ButtonBack: React.FC = () => {
    return (
        <div onClick={() => history.goBack()} className="back"><i className="fas fa-chevron-left"></i></div>
    );
}