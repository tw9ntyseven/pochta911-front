import React from 'react'

export const Wrapper : React.FC = ({children}) => {
    return (
        <div className="wrapper">
            <div className="container">
                {children}
            </div>
        </div>
    );
}