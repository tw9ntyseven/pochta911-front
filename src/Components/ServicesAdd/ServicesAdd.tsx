import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { FetchStatus } from '../../common/types';
import { addserv } from '../../redux/actions/addserv';
import { State } from '../../redux/redux';
import { AddServState } from '../../redux/types/addserv';
import { Alert } from '../Alert/Alert';

interface IServiceAdd {
    onClick: React.MouseEventHandler<HTMLElement>,
}
interface AddServiceState extends AddServState, IServiceAdd {
    addserv: (data: any) => {};
}

export const ServicesAddComponent: React.FC<AddServiceState> = ({error, fetchStatus, onClick, addserv}) => {
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');

    const saveServ = () => {
        let priceNum = Number(price);
        addserv({
            name,
            price: priceNum,
        });
    }
 
    return (
        <div className="export-excel__layer">
        {fetchStatus === FetchStatus.ERROR ?
            <Alert message={error}/>
        : null}
        <div style={{height: '220px'}} className="export-excel">
            <div className="flex export-excel__title-block">
                <div className="home__title">Экспорт в эксель</div>
                <div className="export-excel__btn" onClick={onClick}><i className="fas fa-times"></i></div>
            </div>
            <div className="flex-column export-excel__inputs">
                <div style={{marginBottom: '5px'}} className="home__title">Заголовок услуги в меню</div>
                <input onChange={e => setName(e.target.value)} style={{marginBottom: '15px'}} type="text" className="ipt"/>
                <div style={{marginBottom: '5px'}} className="home__title">Стоимость услуги</div>
                <input onChange={e => setPrice(e.target.value)} type="number" className="ipt"/>
            </div>
            <button onClick={e => saveServ()} className="btn">Сохранить</button>
        </div>
    </div>
    );
       
}

export const ServicesAdd = connect(
    ({ addservice }: State) => ({ ...addservice }),
    (dispatch) => {
        return {
            addserv: (data: any) => {
                return dispatch(addserv(data));
            }
        }
    }
)(ServicesAddComponent)