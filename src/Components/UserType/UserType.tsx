import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { FetchStatus } from '../../common/types';
import { usertype } from '../../redux/actions/usertype';
import { State } from '../../redux/redux';
import { GetUserTypeState } from '../../redux/types/usertype';

interface UserTypeState extends GetUserTypeState {
    usertype: (data: any) => {};
    userTypeId: string;
    onChange: (e: any) => void;
}

export const UserTypeComponent : React.FC<UserTypeState> = ({ error, fetchStatus, usertype, userTypeData, userTypeId, onChange }) => {

    useEffect(() => {
        usertype({});
    }, []);
     
    useEffect(() => {
        if (fetchStatus === FetchStatus.ERROR) {
            console.log(error);
        }
    }, [])


    if (fetchStatus === FetchStatus.FETCHED)  {
        return (
            <>
                <select defaultValue={userTypeId} onChange={onChange} className="ipt btn__right create-cour__select">
                    {userTypeData.data.userTypes ? userTypeData.data.userTypes.map(({id, name}: any) => (
                        <option value={id} key={id}>{name}</option>
                    ))
                    : "Тип пользователя не выбран"}
                </select>
            </>
        );
    } else {
        return null
    }
}
export const UserType = connect(
    ({ getusertype }: State) => ({ ...getusertype }),
    (dispatch) => {
        return {
            usertype: (data: any) => {
                return dispatch(usertype(data))
            }
        }
    }
)(UserTypeComponent)