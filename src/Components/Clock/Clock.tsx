import React, { useEffect, useMemo, useState } from 'react';
import moment from 'moment';

export const Clock: React.FC = () => {
    const [dateTimeNowUTC, setDateTimeNowUTC] = useState<Date>(new Date());

    const time = useMemo(() => {
        return moment(dateTimeNowUTC).format("HH:mm:ss");
    }, [moment(dateTimeNowUTC).format("HH:mm:ss")])


    const tick = () => {
        setDateTimeNowUTC(new Date())
    };

    useEffect(() => {
        let interval = setInterval(
            () => {
                tick();
            }
        );
        return () => {
            clearInterval(interval);
        }
    }, []);

    return (
        <div className="create-order__date"><i className="fas fa-clock mar"></i>{time}</div>
    )
}