import React, { useCallback, useEffect, useState } from 'react'
import { getFingerprint } from "../../Utils";
import { Alert } from '../Alert/Alert';
import { FetchStatus } from '../../common/types';
import { AuthorizationState } from '../../redux/types/auth';
import { connect } from 'react-redux';
import { State } from '../../redux/redux';
import { auth } from '../../redux/actions/auth';
import { history } from '../../redux/reducers';

interface AuthState extends AuthorizationState {
    auth: (data: any) => {};
}

export const AuthComponent : React.FC<AuthState> = ({error, fetchStatus, auth, isAuth}) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [hiddenValue, setHiddenValue] = useState(true);
    const [message, setMessage] = useState('');
    const [errorMessage, setErrorMessage] = useState(false);

    // Fingerprint and IP
    const [fingerprint, setFingerprint] = useState(null);
    const [ipData, setIpData] = useState(null);
    const [showReport] = useState(true);

    useEffect(() => {
        if (showReport) {
          fetch("https://extreme-ip-lookup.com/json")           // Get the IP data
            .then(res => res.json())
            .then(ip => Promise.all([ip, getFingerprint()]))    // Get the fingerprint
            .then(([ip, fp]:any) => {
              setFingerprint(fp[0].value);                               // Update the state
              setIpData(ip.query);
            });
        }
      }, [showReport]);

    const loginChangeCallback = useCallback(
        (e: any) => {
            setLogin(e.target.value);
        }, []
    );
    const passwordChangeCallback = useCallback(
        (e: any) => {
            setPassword(e.target.value);
        }, []
    );
    const toggleShow = useCallback( 
        (e: any) => {
            setHiddenValue(!hiddenValue)
        }, [hiddenValue]
    );
        console.log(isAuth, "isAuth");
        
    const Submit = async(e: React.MouseEvent<HTMLElement>) =>{
        if (login === "" && password === "") {
            setErrorMessage(!errorMessage);
            setMessage('Заполните все обязательные поля!');
        } else {
            auth({
                login,
                password,
                fingerprint,
                ip:ipData,
            });
        } 
    }  

        return (
            <>
            {isAuth ?
            history.push("/logistics")
            :null}
            {isAuth ?
            window.location.reload()
            :null}
            {fetchStatus === FetchStatus.ERROR ?
                <Alert message={error}/>
            : null}
            {errorMessage ?
                <Alert message={message} />
            :null}
            <span className="home__title">Логин</span>
            <input onChange={loginChangeCallback} type="text" className="reg__ipt" required/>
            <span className="home__title">Пароль</span>
            <div className="flex">
                <input onChange={passwordChangeCallback} type={hiddenValue ? "password" : "text"} className="reg__ipt" required/>
                <div onClick={toggleShow} className="reg__hide"><i className="fas fa-eye"></i></div>
            </div>
            <button onClick={Submit} className="btn reg__btn">Войти</button>
            </>
        );
}

export const Auth = connect(
    ({ authorization }: State) => ({ ...authorization }),
    (dispatch) => {
        return {
            auth: (data: any) => {
                return dispatch(auth(data))
            }
        }
    }
)(AuthComponent)