import React from 'react'
import './CreateCourIpt.css'

type CreateCourIptProps = {
    title: string,
    type: string,
    disabled?: boolean,
}

export const CreateCourIpt = ({title, type, disabled}: CreateCourIptProps) => (
    <div className="create-cour__flex">
        <span className="home__title create-cour__subtitle">{title}{disabled ? null : <i className="fas fa-star-of-life main mar-l"></i>}</span>
        <input type={type} className="ipt btn__right create-cour__ipt"/>
    </div>
)