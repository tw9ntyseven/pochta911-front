import React, {useEffect} from 'react';
import './App.css';
import {Redirect, Route, Switch, useLocation} from 'react-router-dom';
// Redux
import { history } from '../../redux/reducers';
import { ConnectedRouter } from 'connected-react-router';
// Screens
import {Home} from '../../Screens/Home/Home';
import {Nav} from '../Nav/Nav';
import {Auto} from '../../Screens/Auto/Auto';
import {Clients} from '../../Screens/Clients/Clients';
import {Collaborator} from '../../Screens/Collaborator/Collaborator';
import {Notification} from '../../Screens/Notification/Notification';
import {CreateOrder} from '../../Screens/CreateOrder/CreateOrder';
import {Reg} from '../../Screens/Reg/Reg';
import {CreateCourier} from '../../Screens/CreateCourier/CreateCourier';
import {CreateClient} from '../../Screens/CreateClient/CreateClient';
import {AdminPanel} from '../../Screens/AdminPanel/AdminPanel';
import {PricePanel} from '../../Screens/PricePanel/PricePanel';
import {TimePanel} from '../../Screens/TimePanel/TimePanel';
import {ProductsPanel} from '../../Screens/ProductsPanel/ProductsPanel';
import {ServicesPanel} from '../../Screens/ServicesPanel/ServicesPanel';
import {CourierPays} from '../../Screens/AdminPanel/CourierPays/CourierPays';
import {TgJournal} from '../../Screens/AdminPanel/TgJournal/TgJournal';
import {ViberJournal} from '../../Screens/AdminPanel/ViberJournal/ViberJournal';
import {GroupList} from '../../Screens/AdminPanel/GroupList/GroupList';
import {GroupLaw} from '../../Screens/AdminPanel/GroupList/GroupLaw/GroupLaw';
import { NotWorking } from '../../Screens/AdminPanel/NotWorking/NotWorking';
// time and date
import moment from 'moment';
import 'moment/locale/ru';
import { registerLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';
import { Courier } from '../../Screens/AdminPanel/CourierPays/Courier/Courier';
import { CourierViewCard } from '../../Screens/AdminPanel/CourierPays/CourierViewCard/CourierViewCard';
import { PricePanelMan } from '../../Screens/PricePanelMan/PricePanelMan';
import { TimePanelMan } from '../../Screens/TimePanelMan/TimePanelMan';


moment.locale("ru");
registerLocale('ru', ru)

function ScrollToTop(props : any) {
    const {pathname} = useLocation();
    useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);
    return props.children
}
const token = localStorage.getItem("AccessToken") as string;
// console.log(token, "token");



export const App = () => {
        return (
            <ConnectedRouter history={history}>
                        {
                            token === null && <Redirect to="/registration" />
                        }
                <ScrollToTop>
                    <Nav/>
                    <Switch>
                        <Route component={Home} path="/logistics" exact/>
                        <Route component={Auto} path="/auto" exact/>
                        <Route component={Clients} path="/clients" exact/>
                        <Route component={Collaborator} path="/collaborator" exact/>
                        <Route component={Notification} path="/notification" exact/>
                        <Route component={CreateOrder} path="/logistics/create-order" exact/>
                        <Route component={Reg} path="/registration" exact/>
                        <Route component={CreateCourier} path="/auto/create-courier" exact/>
                        <Route component={CreateClient} path="/clients/client" exact/>
                        <Route component={AdminPanel} path="/admin" exact/>
                        <Route component={PricePanel} path="/price-panel" exact/>
                        <Route component={TimePanel} path="/time-panel" exact/>
                        <Route component={ProductsPanel} path="/products-panel" exact/>
                        <Route component={ServicesPanel} path="/services-panel" exact/>
                        <Route component={CourierPays} path="/admin/get-cars-calc" exact/>
                        <Route component={TgJournal} path="/admin/tg-journal" exact/>
                        <Route component={ViberJournal} path="/admin/viber-journal" exact/>
                        <Route component={GroupList} path="/admin/groups" exact/>
                        <Route component={GroupLaw} path="/admin/groups/groups-law" exact/>
                        <Route component={NotWorking} path="/admin/not-working" exact />
                        <Route component={Courier} path="/admin/get-cars-calc/courier" exact/>
                        <Route component={CourierViewCard} path="/admin/get-cars-calc/courier/courier-view-card" exact/>
                        <Route component={PricePanelMan} path="/admin/price-panel/man" exact/>
                        <Route component={TimePanelMan} path="/admin/time-panel/man" exact/>
                    </Switch>
                </ScrollToTop>
            </ConnectedRouter>
        );
}