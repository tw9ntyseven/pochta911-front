import React from 'react'

interface IItemDeliveryTime {
    inTime: boolean,
    disabled?: boolean,
}

export const ItemDeliveryTime: React.FC<IItemDeliveryTime> = ({inTime, disabled}) => {
    return (
        <div className="flex">
                <div className="flex-column delivery-address__timeline">
                    <div className="home__title delivery-address__title">Прибыть с</div>
                    <div className="flex">
                        <div className="home__title"><i className="fas fa-clock mar"></i></div>
                        <select disabled={disabled} className="mar edit-block__ipt delivery-address__ipt">
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                        </select>
                        <div className="mar home__title">:</div>
                        <select disabled={disabled} className="edit-block__ipt delivery-address__ipt">
                            <option value="00">00</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                        </select>
                    </div>
                </div>
                <div className="flex-column delivery-address__timeline">
                    <div className="home__title delivery-address__title">Прибыть по</div>
                    <div className="flex">
                        <div className="home__title"><i className="fas fa-clock mar"></i></div>
                        <select disabled={disabled} className="mar edit-block__ipt delivery-address__ipt">
                            <option value="00">00</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                        </select>
                        <div className="mar home__title">:</div>
                        <select disabled={disabled} className="edit-block__ipt delivery-address__ipt">
                            <option value="00">00</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                        </select>
                    </div>
                </div>
                {inTime ?
                    <label className="flex btn__right">
                        <input style={{marginRight: '10px'}} type="checkbox" className="checkbox-20" />
                        <div className="create-order__radio-item-text">К точному времени</div>
                    </label>
                    :null}
            </div>
    );
}