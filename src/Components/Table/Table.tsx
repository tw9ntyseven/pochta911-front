import React from 'react'
import './Table.css'

type ITableMapProps = {
    items: Array<any>,
}

const TableMap = ({items}: ITableMapProps) => {
    return (
    <tbody>
        {items.map((item, index) => (
            <tr key={index} className="table-map">
                <td className="table-map__item">{item.numberOrder}</td>
                <td className="table-map__item">{item.doneOrder}</td>
                <td className="table-map__item">{item.timeDelivery}</td>
                <td className="table-map__item">{item.auto}<i className="fas fa-car-side"></i></td>
                <td className="table-map__item">
                    {item.address}<br/>
                    {item.phone}<br/>
                    <i>{item.info}</i>
                </td>
                <td className="table-map__item">{item.company}</td>
                <td className="bold table-map__item">{item.deliveryAddress}</td>
                <td className="table-map__item">{item.status}</td>
                <td className="table-map__item">
                    {item.courier}<br/>
                    {item.courierPhone}
                </td>
                <td className="table-map__item">{item.deliveryPrice}</td>
                <td className="table-map__item">{item.incassation}</td>
                <td className="table-map__item">{item.acceptOrder}</td>
                <td className="table-map__item"><i style={{fontSize: '18px'}} className="fas fa-redo"></i></td>
            </tr>
        ))}
    </tbody>
    );
}

export const Table : React.FC = () => {
    return (
        <table>
            <thead>
                <tr className="table__title-overlay">
                    <th className="table__title">Заказ</th>
                    <th className="table__title">Готов</th>
                    <th className="table__title">Время доставки</th>
                    <th className="table__title">Пеш</th>
                    <th className="table__title">Адрес приема и контакт</th>
                    <th className="table__title">Компания</th>
                    <th className="table__title">Адрес доставки</th>
                    <th className="table__title">Статус</th>
                    <th className="table__title">Курьер и телефон</th>
                    <th className="table__title">Стоимость доставки</th>
                    <th className="table__title">Инкассация</th>
                    <th className="table__title">Прим. заказ</th>
                    <th className="table__title">Повторить</th>
                </tr>
            </thead>
            <TableMap items={[
                {
                    numberOrder: '100',
                    doneOrder: '7:50 - 6:40',
                    timeDelivery: '7:50 - 6:40',
                    auto: true,
                    address: 'ул. Черноморская 56',
                    phone: '[797878787878]',
                    info: 'Информация полезная для проверки вместимости текста в таблицу! проверки вместимости текста в таблицу!',
                    company: 'Helix',
                    deliveryAddress: 'ул. Черноморская 56',
                    status: 'Активный',
                    courier: 'Иван Иванович',
                    courierPhone: '[797811288228]',
                    deliveryPrice: '300р',
                    incassation: '0.00',
                    acceptOrder: 'информация для подтверждения'
                },
                {
                    numberOrder: '100',
                    doneOrder: '7:50 - 6:40',
                    timeDelivery: '7:50 - 6:40',
                    auto: true,
                    address: 'ул. Черноморская 56',
                    phone: '[797878787878]',
                    info: 'Информация полезная для проверки вместимости текста в таблицу! проверки вместимости текста в таблицу!',
                    company: 'Helix',
                    deliveryAddress: 'ул. Черноморская 56',
                    status: 'Активный',
                    courier: 'Иван Иванович',
                    courierPhone: '[797811288228]',
                    deliveryPrice: '300р',
                    incassation: '0.00',
                    acceptOrder: 'информация для подтверждения'
                }
                ]} />
        </table>
    );
}