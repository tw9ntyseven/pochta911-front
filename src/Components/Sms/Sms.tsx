import React, { useCallback, useState } from 'react'
import { Link } from 'react-router-dom';
import { SmsWindowState } from '../../redux/types/sms';
import { Alert } from '../Alert/Alert';
import { FetchStatus } from '../../common/types';
import { connect } from 'react-redux';
import { State } from '../../redux/redux';
import { sms } from '../../redux/actions/sms';
import { history } from '../../redux/reducers';

interface ISms {
    onClick: React.MouseEventHandler<HTMLElement>,
    userId: string,
}
interface SmsState extends SmsWindowState, ISms {
    sms: (data: any) => {};
}

export const SmsComponent : React.FC<SmsState> = ({onClick, userId, error, fetchStatus, sms, isReg}) => {
    const [smsCode, setSmsCode] = useState("");
    const [message, setMessage] = useState('');
    const [errorMessage, setErrorMessage] = useState(false);


    const smsCodeChangeCallback = useCallback(
        (e: any) => {
            setSmsCode(e.target.value);
        }, []
    );

    const Submit = async(e: React.MouseEvent<HTMLElement>) => {   
        if (smsCode === "") {
            setErrorMessage(!errorMessage);
            setMessage('Заполните все обязательные поля!');
        } else {
            sms({
                userId,
                smsCode
            });
        }
    }
    return (
        <div className="export-excel__layer">
            {/* {isReg ?
            history.push("/registration")
            :null} */}
            {isReg ?
            <Alert message="Теперь вы можете войти в аккаунт!" />
            :null}
            {fetchStatus === FetchStatus.ERROR ?
                <Alert message={error}/>
            : null}
            {errorMessage ?
                <Alert message={message} />
            :null}
        <div style={{height: '180px'}} className="export-excel">
            <div className="flex export-excel__title-block">
                <div className="home__title">Подтвердите телефон</div>
                <div className="export-excel__btn" onClick={onClick}><i className="fas fa-times"></i></div>
            </div>
                <div style={{marginTop: '10px'}} className="home__title">Введите 4-х значный код</div>
                <input onChange={smsCodeChangeCallback} style={{marginTop: '5px'}} type="number" className="ipt"/>
                <Link style={{marginTop: '5px', fontSize: '12px', textDecoration: 'none', color: '#000'}} to="#">повторить отправку</Link>
                <button onClick={Submit} style={{marginTop: '15px'}} className="btn">Продолжить</button>
        </div>
    </div>
    );
}
export const Sms = connect(
    ({ smsWindow }: State) => ({ ...smsWindow }),
    (dispatch) => {
        return {
            sms: (data: any) => {
                return dispatch(sms(data))
            }
        }
    }
)(SmsComponent)