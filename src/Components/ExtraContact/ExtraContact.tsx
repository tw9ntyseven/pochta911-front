import React, { useState } from 'react'
import { UserState } from '../../Screens/CreateClient/CreateClient';
import { Alert } from '../Alert/Alert';
import './ExtraContact.css';

interface IExtraContact extends UserState {
    additionalContactsAdd: any,
    additionalContactsDel: any,
    save: any,
}

export const ExtraContact : React.FC<IExtraContact> = ({userData, additionalContactsAdd, additionalContactsDel, save}) => {
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [identeficator, setIdenteficator] = useState('');

    const [showMessage, setShowMessage] = useState(false);
    const [message, setMessage] = useState('');

    const AddContact = () => {
        if (name == "" && phone == "") {
            setShowMessage(!showMessage);
            setMessage("Заполните все поля!");
        } else {
        additionalContactsAdd.push({
            name,
            phone,
        })
        // setMessage("Добавлено, для сохранения нажмите СОХРАНИТЬ");
        // setShowMessage(!showMessage);
        save();
    }
    }

    const DelContact = (id: string) => {
        setIdenteficator(id);
        additionalContactsDel.push({
            id,
        })
        // setMessage("Удалено, для сохранения нажмите СОХРАНИТЬ");
        // setShowMessage(!showMessage);
        save();
    }

    return (
        <>
        {showMessage ?
            <Alert message={message} />
        :null}
        <div  style={{marginBottom: '15px'}} className="extra-contact">
        <div style={{marginBottom: '10px'}} className="flex">
            <input onChange={e => setPhone(e.target.value)} style={{width: '40%'}} placeholder="Телефон" type="number" className="ipt mar create-cour__ipt"/>
            <input onChange={e => setName(e.target.value)} style={{width: '60%'}} placeholder="Имя, должность" type="text" className="ipt create-cour__ipt"/>
        </div>
        <button onClick={e => AddContact()} className="btn btn__right">Добавить</button>
        </div>
        { userData.data.additionalContacts.length !== 0 ? userData.data.additionalContacts.map(({id, name, phone}: any) => (
        <div style={{marginBottom: '10px'}} key={id} className="extra-contact extra-contact__item">
        <div onClick={e => DelContact(id)} className="extra-contact__close"><i className="fas fa-times"></i></div>
        <div style={{marginBottom: '10px'}} className="flex">
            <div className="bold favorite-address__text">Телефон:</div>
            <div className="btn__right bold favorite-address__text">{phone}</div>
        </div>
        <div style={{marginBottom: '10px'}} className="flex">
        <div className="bold favorite-address__text">Имя, должность:</div>
        <div className="btn__right bold favorite-address__text">{name}</div>
        </div>
        </div>
        )): 'Еще нет дополнительных контактов'}
        </>
    );
}