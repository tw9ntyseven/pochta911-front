import React from 'react'
import './BurgerMenu.css'
import { Link } from 'react-router-dom';

function handleClick(e: React.MouseEvent<HTMLElement>) {
    var foo = document.querySelectorAll(".footer__item");

    for (let i = 0; i < foo.length; i++) {
      foo[i].classList.remove("footer__item--active");
    }
    e.currentTarget.classList.add("footer__item--active");
  };

interface IBurgerMenu {
    isShowBmenuChangeCallback: (e: React.MouseEvent<HTMLElement>) => void,
    isShowBmenu: boolean,
}

export const BurgerMenu : React.FC<IBurgerMenu> = ({isShowBmenuChangeCallback, isShowBmenu}) => {
    return (
        <div onClick={isShowBmenuChangeCallback} className="burger-menu__layer">
            <div className={`burger-menu ${isShowBmenu ? '' : 'burger-menu--close'}`}>
                <div onClick={isShowBmenuChangeCallback} className="btn__right export-excel__btn burger-menu__close"><i className="fas fa-times"></i></div>
                <div className="flex-column">
                <Link onClick={handleClick} to="/admin" className="flex footer__item burger-menu__item">
                <i className="fas fa-briefcase mar"></i>
                Админ
                </Link>
                <Link onClick={handleClick} to="/price-panel" className="flex footer__item burger-menu__item">
                <i className="far fa-money-bill-alt mar"></i>
                Стоимость
                </Link>
                <Link onClick={handleClick} to="/time-panel" className="flex footer__item burger-menu__item">
                <i className="fas fa-clock mar"></i>
                Времена
                </Link>
                <Link onClick={handleClick} to="/products-panel" className="flex footer__item burger-menu__item">
                <i className="fas fa-store mar"></i>
                Товары
                </Link>
                <Link onClick={handleClick} to="/admin/price-panel/man" className="flex footer__item burger-menu__item">
                <i className="far fa-money-bill-alt mar"></i>
                Стоимость (пешеход)
                </Link>
                <Link onClick={handleClick} to="/admin/time-panel/man" className="flex footer__item burger-menu__item">
                <i className="fas fa-clock mar"></i>
                Времена (пешеход)
                </Link>
                <Link onClick={handleClick} to="/services-panel" className="flex footer__item burger-menu__item">
                <i className="fas fa-ellipsis-h mar"></i>
                Доп. услуги
                </Link>
            </div>
            
            </div>
        </div>
    );
}