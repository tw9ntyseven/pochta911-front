import React from 'react'
import './Alert.css'

interface IAlert {
    message: string,
}

export const Alert: React.FC<IAlert> = ({message}) => {
    return (
        <div className="flex alert">
            <div className="alert__text">{message}</div>
        </div>
    );
}