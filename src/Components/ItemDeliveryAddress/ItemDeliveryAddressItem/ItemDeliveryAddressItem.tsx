import React, { useState } from 'react';
import { ItemDeliveryTime } from '../../ItemDeliveryTime/ItemDeliveryTime';
import { DeliveryAddress } from '../ItemDeliveryAddress';

interface IItemDeliveryAddressItem {
    items: Array<any>;
}

export const ItemDeliveryAddressItem: React.FC<IItemDeliveryAddressItem> = ({ inTime, index, items }: any) => {
    const [addItem, setAddItem] = useState();
    return (
        <>
        <div className="create-order__edit-block delivery-address__item">
            <div className="flex delivery-address__layer">
                <div className="delivery-address__num mar">{index + 1}</div>
                <div className="mar home__title">Любимые адреса:</div>
                <select placeholder="г. Санкт-Петербург" className="delivery-address__select">
                    <option value="г. Санкт-Петербург">г. Санкт-Петербург</option>
                </select>
                <div className="btn__right flex">
                    <div style={{backgroundColor: "#d9534f"}} className="delivery-address__num"><i className="fas fa-times"></i></div>
                </div>
            </div>
            <div className="flex">
            <div style={{width: '50%', marginRight: '10px'}} className="flex-column">
                <span className="home__title">Адрес отправления</span>
                <input placeholder="г. Санкт-Петербург" type="text" className="edit-block__ipt"/>
            </div>
            <div style={{width: '16.6%', marginRight: '10px'}} className="flex-column">
                <span className="home__title">Подъезд</span>
                <input type="text" className="edit-block__ipt"/>
            </div>
            <div style={{width: '16.6%', marginRight: '10px'}} className="flex-column">
                <span className="home__title">Этаж</span>
                <input type="number" className="edit-block__ipt"/>
            </div>
            <div style={{width: '16.6%'}} className="flex-column">
                <span className="home__title">кв/офис/помещ</span>
                <input type="text" className="edit-block__ipt"/>
            </div>
            </div>
            <div className="flex">
                <div style={{width: '60%', marginRight: '10px'}} className="flex-column">
                    <span className="home__title">Получатель ФИО</span>
                    <input type="text" className="edit-block__ipt"/>
                </div>
                <div style={{width: '40%'}} className="flex-column">
                    <span className="home__title">Телефон получателя</span>
                    <input type="text" className="edit-block__ipt"/>
                </div>
            </div>
            {/* Time */}
            <ItemDeliveryTime inTime={true} />
            <div className="flex">
                <textarea placeholder="Примечания к заказу" className="edit-block__ipt edit-block__ipt--textarea delivery-address__textarea"/>
            </div>

            {/* Added Items */}
            <DeliveryAddress items={[{}]} />

            {/* Add Item Button */}
            <div className="mar-top center">
                <div onClick={e => setAddItem(items.push(addItem))} style={{cursor: 'pointer'}} className="home__title"><i style={{color: '#4cae4c'}} className="fas fa-plus-circle mar"></i>Добавить груз</div>
            </div>

            <div style={{marginTop: '20px'}} className="flex">
            <div style={{marginRight: '10px'}} className="home__title">Дополнительные услуги</div>
                <input style={{width: '400px'}} list="attendance" placeholder="Выбрать услугу" type="text" className="create-order__ipt"/>
                <datalist id="attendance">
                    <option value="SMS информирование заказчика">SMS информирование заказчика</option>
                    <option value="Подписать сопроводительные документы">Подписать сопроводительные документы</option>
                </datalist>
            </div>
            <div style={{marginTop: '25px'}} className="flex-column edge__bottom">
                <div className="flex">
                <div style={{width: '46%', marginRight: '10px'}} className="flex-column">
                    <span className="home__title">Получатель оплату за товар</span>
                    <select className="edit-block__ipt delivery-address__select-item">
                        <option value="">Отправителем</option>
                        <option value="">Получателем</option>
                        <option value="">Вычитается из полученных наличных</option>
                        <option value="">По договору</option>
                    </select>
                </div>
                <div style={{width: '20%', marginRight: '10px'}} className="flex-column">
                    <span className="home__title">Сумма</span>
                    <input type="number" className="edit-block__ipt"/>
                </div>
                <div style={{width: '33%'}} className="flex-column">
                    <span className="home__title">Каким способом</span>
                    <select className="edit-block__ipt delivery-address__select-item">
                        <option value="">Банковская карта</option>
                        <option value="">Наличными без чека</option>
                        <option value="">Наличными с чеком</option>
                    </select>
                </div>
            </div>
            <div className="flex">
            <div style={{width: '40%', marginRight: '10px'}} className="flex-column">
                    <span className="home__title">Объявленная ценность</span>
                    <select className="edit-block__ipt delivery-address__select-item">
                        <option value="">Отправителем</option>
                        <option value="">Получателем</option>
                        <option value="">Вычитается из полученных наличных</option>
                        <option value="">По договору</option>
                    </select>
                </div>
                <div style={{width: '25%', marginRight: '20px'}} className="flex-column">
                    <span className="home__title">Сумма</span>
                    <input type="number" className="edit-block__ipt"/>
                </div>
                <div style={{width: '35%'}} className="flex">
                <div className="home__title">Стоимость адреса:</div>
                <h1 style={{fontSize: '17px', color: "green"}} className="home__title">400 руб.</h1>
                </div>
            </div>
            <div className="flex">

            </div>
            </div>
        </div>
    </>
    )
}