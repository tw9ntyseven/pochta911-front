import React, { useState } from 'react'

export const DeliveryAddressItem: React.FC = ({added}: any) => {
    const [customProps, setCustomProps] = useState<boolean>(false);

    return (
        <div className="flex-column delivery-address__layered">
                {/* <div className="flex">
                    <div className="delivery-address__help">Подсказка</div>
                </div> */}
                <div className="flex">
                    <div style={{width: '60%', marginRight: '10px'}} className="flex-column">
                        <span className="home__title">Что везем</span>
                        <select onChange={e => setCustomProps(!customProps)} placeholder="Посылка" className="edit-block__ipt delivery-address__select-item">
                            <option value="Сверток до 5кг">Сверток до 5кг</option>
                            <option value="Сверток до 5кг">Коробка/Пакет до 10кг</option>
                            <option value="Сверток до 5кг">Коробка/Пакет до 15кг</option>
                            <option value="">Свои параметры груза</option>
                        </select>
                    </div>
                    <div style={{width: '33%'}} className="flex-column">
                        <span className="home__title">Количество</span>
                        <input placeholder="10" type="number" className="edit-block__ipt"/>
                    </div>
                    {added ? <div style={{width: '5%'}} className="flex"><div style={{backgroundColor: "#d9534f"}} className="btn__right delivery-address__num"><i className="fas fa-times"></i></div></div>:null}
                </div>
               {customProps ? <div className="flex">
                    <div style={{width: '30%', marginRight: '10px'}} className="flex-column">
                        <span className="home__title">Длина, (см)</span>
                        <input type="number" className="edit-block__ipt"/>
                    </div>
                    <div style={{width: '33%', marginRight: '10px'}} className="flex-column">
                        <span className="home__title">Ширина, (см)</span>
                        <input type="number" className="edit-block__ipt"/>
                    </div>
                    <div style={{width: '33%'}} className="flex-column">
                        <span className="home__title">Высота, (см)</span>
                        <input type="number" className="edit-block__ipt"/>
                    </div>
                </div>:null}
            </div>
    );
}