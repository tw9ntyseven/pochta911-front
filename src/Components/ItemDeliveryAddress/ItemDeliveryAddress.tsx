import React from 'react'
import { DeliveryAddressItem } from './DeliveryAddressItem/DeliveryAddressItem';
import './ItemDeliveryAddress.css'
import { ItemDeliveryAddressItem } from './ItemDeliveryAddressItem/ItemDeliveryAddressItem';

interface IItemDeliveryAddress {
    items: Array<any>;
}

export const ItemDeliveryAddress : React.FC<IItemDeliveryAddress> = ({items}) => {
    return (
        <div className="delivery-address">
            {items.map((item, index) => (
                <ItemDeliveryAddressItem key={index} index={index} {...item}/>
            ))}
        </div>
    );
}

export const DeliveryAddress : React.FC<IItemDeliveryAddress> = ({items}) => {
    return (
        <>
            {items.map((item, index) => (
                <DeliveryAddressItem added={false} key={index} index={index} {...item}/>
            ))}
        </>
    );
}