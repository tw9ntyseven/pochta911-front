import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { FetchStatus } from '../../common/types';
import { termofpayment } from '../../redux/actions/termofpayment';
import { State } from '../../redux/redux';
import { GetTermOfPaymentState } from '../../redux/types/termofpayment';
import { Alert } from '../Alert/Alert';

interface TermsOfPaymentState extends GetTermOfPaymentState {
    termofpayment: (data: any) => {};
    onChange: (e: any) => void;
    termOfPayment: string;
}

const TermsOfPaymentComponents : React.FC<TermsOfPaymentState> = ({ error, fetchStatus, termofpayment, termOfPaymentData, termOfPayment, onChange }) => {
    
    useEffect(() => {
        termofpayment({});
    }, []);

    return (
        <>
        {fetchStatus === FetchStatus.ERROR ?
            <Alert message={error}/>
        : null}
            <select defaultValue={termOfPayment} onChange={onChange} className="ipt btn__right create-cour__select">
                {termOfPaymentData ? termOfPaymentData?.data?.termsOfPayment.map(({id, name}: any) => (
                    <option value={id} key={id}>{name}</option>
                ))
                : "Условия оплаты не выбран"}
            </select>
        </>
    );
}
export const TermsOfPayment = connect(
    ({ gettermofpayment }: State) => ({ ...gettermofpayment }),
    (dispatch) => {
        return {
            termofpayment: (data: any) => {
                return dispatch(termofpayment(data))
            }
        }
    }
)(TermsOfPaymentComponents)