import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { FetchStatus } from '../../common/types';
import { paytype } from '../../redux/actions/paytype';
import { State } from '../../redux/redux';
import { GetPayTypeState } from '../../redux/types/paytype';

interface PayTypeState extends GetPayTypeState {
    paytype: (data: any) => {};
    onChange: (e: any) => void;
    value: string;
    payTypeId: string;
}

export const PayTypeComponent : React.FC<PayTypeState> = ({error, fetchStatus, paytype, payTypeData, onChange, value, payTypeId}) => {

    useEffect(() => {
        paytype({});
    }, []);

    useEffect(() => {
        if (fetchStatus === FetchStatus.ERROR) {
            console.log(error);
        }
    }, [])
    console.log(value, "VALUE");
    

    if (fetchStatus === FetchStatus.FETCHED)  {
        return (
        <select name="payType" id="payType" defaultValue={payTypeId} onChange={onChange} className="ipt btn__right create-cour__select">
            {payTypeData ? payTypeData.data.payTypes.map(({id, name}: any) => (
                <option key={id} value={id}>{name}</option>
            ))
            : "Тип оплаты не выбран"}
        </select>
    );
    } else {
        return null
    }
}
export const PayType = connect(
    ({ getpaytype }: State) => ({ ...getpaytype }),
    (dispatch) => {
        return {
            paytype: (data: any) => {
                return dispatch(paytype(data))
            }
        }
    }
)(PayTypeComponent)
