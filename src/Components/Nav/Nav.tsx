import React, { useCallback, useState } from 'react'
import './Nav.css'
import { Link } from 'react-router-dom'
import { BurgerMenu } from '../BurgerMenu/BurgerMenu';
import jwt from "jsonwebtoken";


function handleClick(e: React.MouseEvent<HTMLElement>) {
    var foo = document.querySelectorAll(".nav__links-item");

    for (let i = 0; i < foo.length; i++) {
      foo[i].classList.remove("nav__links-item--active");
    }
    e.currentTarget.classList.add("nav__links-item--active");
};

const token = localStorage.getItem("AccessToken") as string;
// const getUserId = (token:string) => {
//     const data = jwt.decode(token) as {
//         exp: number,
//         userId: string
//     }
//     if (data?.userId) {
//         return data?.userId
//     } else {
//         alert(`userId ${data?.userId}`);
//     }
// }


export const Nav: React.FC = () => {


    const [showBmenu, setShowBmenu] = useState(false);

    const ShowMenu = useCallback(
        (e: React.MouseEvent<HTMLElement>) => {
            setShowBmenu(!showBmenu)
        },
        [showBmenu],
    );
    if(token === null) {
        return null
    } else {
    return (
        <div className="nav">
            <div className="container nav__container">
                <span onClick={ShowMenu} className="burger-btn"><i className="fas fa-bars"></i></span>
                    {showBmenu ?
                    <BurgerMenu isShowBmenu={showBmenu} isShowBmenuChangeCallback={ShowMenu} />
                    :null}
                <img src={require('../../Img/logo.png')} alt="logo" className="nav__logo"/>
                <div className="nav__links">
                    <Link to="/logistics" onClick={handleClick} className="nav__links-item">
                    <i className="fas fa-bus-alt"></i> Логистика
                    </Link>
                    <Link to="/auto" onClick={handleClick} className="nav__links-item">
                    <i className="fas fa-car"></i> Автоштат
                    </Link>
                    <Link to="/clients" onClick={handleClick} className="nav__links-item">
                    <i className="fas fa-user"></i> Клиенты
                    </Link>
                    <Link to="/collaborator" onClick={handleClick} className="nav__links-item">
                    <i className="fas fa-user-friends"></i> Сотрудники
                    </Link>
                    <Link to="/notification" onClick={handleClick} className="nav__links-item">
                    <i className="fas fa-envelope"></i> Оповещения
                    </Link>
                    <Link to="/clients/client/" onClick={handleClick} className="nav__links-item">
                    <i className="fas fa-user-circle"></i> Профиль
                    </Link>
                    {/* <Link to="#" onClick={handleClick} className="nav__links-item nav__links-item--out">
                    <i className="fas fa-sign-out-alt"></i> Выход
                    </Link> */}
                </div>
                <div className="nav__phone">
                <i className="fas fa-phone"></i> +7(812)242-80-81
                </div>
            </div>
        </div>
    );
}
}