import { BaseRestDataProvider } from "./BaseRestDataProvider";
import axios from 'axios';
import jwt from "jsonwebtoken";

const token = localStorage.getItem("AccessToken") as string;
const getUserId = (token:string) => {
    const data =  jwt.decode(token) as {
        exp: number,
        userId: string
    }
    return data.userId
}


export class AuthDataProvider extends BaseRestDataProvider {

    register = async (data: { login: string, password: string, phone: string}) => {

        return axios.post(`${this.host}/user/create`, {
            ...data
        }, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }

    refreshToken = async () => {
        return axios.post(`${this.host}/auth/refreshTokens`, {
        }, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }

    authorization = async (data: { login: string, password: string, fingerprint: string, ip: string}) => {
        return axios.post(`${this.host}/auth`, {
            ...data
        }, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }

    smsWindow = async (data: { userId: string, smsCode: string}) => {
        return axios.post(`${this.host}/sms/accept/reg`, {
            ...data
        }, {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    getuser = async () => {
        return await axios.get(`${this.host}/user/get/${getUserId(token)}`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    getusertype = async () => {
        return await axios.get(`${this.host}/userType/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    gettermofpayment = async () => {
        return await axios.get(`${this.host}/termsOfPayment/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    getpaytype = async () => {
        return await axios.get(`${this.host}/payType/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    changeUser = (data: { nameOwner: string, nameOfCompany: string, phoneInfo: string, phone: string, nameLegal: string, addressLegal: string, agreementNum: string, agreementDate: string, INN: string, KPP: string, BIKBank: string, settlementAccount: string, taxType: string, NDS: string, userTypeId: string, payTypeId: string, termsOfPaymentId: string, emailFinancial: string, emailNotification: string, favoriteAddressesDel: Array<object>, favoriteAddressesAdd: Array<object>, additionalContactsDel: Array<object>, additionalContactsAdd: Array<object>}) => {
        return axios.put(`${this.host}/user/update/${getUserId(token)}`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    getaddservice = async () => {
        return await axios.get(`${this.host}/adminOptions/additionalServices/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    changeAddServ = async (data: { id: string, priсe: number, enable: boolean, name: string }) => {
        return await axios.put(`${this.host}/adminOptions/additionalServices/update`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    addServ = async (data: {name: string, price: number}) => {
        return axios.post(`${this.host}/adminOptions/additionalServices/create`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    delServ = async (id: string) => {
        return await axios.delete(`${this.host}/adminOptions/additionalServices/delete/${id}`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    getPriceForCar = async () => {
        return await axios.get(`${this.host}/adminOptions/price/car/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    changePriceForCar = async (data: { crossingNeva: number, increaseProcent: number, exactTimeEnable: boolean, exactTimePrice: number, outOfCAD: number, outOfCity: number, payBankCardCommissionEnable: boolean, payBankCardCommissionProcent: number, suburbIncreaseKilometers: number, suburbIncreaseProcent: number, vsevolozhsk: number, pricePerKilometerAdd: Array<object>, pricePerKilometerDel: Array<object>}) => {
        return await axios.put(`${this.host}/adminOptions/price/car/update`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    getPriceForMan = async () => {
        return await axios.get(`${this.host}/adminOptions/price/man/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    changePriceForMan = async (data: { crossingNeva: number, increaseProcent: number, exactTimeEnable: boolean, exactTimePrice: number, outOfCAD: number, outOfCity: number, payBankCardCommissionEnable: boolean, payBankCardCommissionProcent: number, suburbIncreaseKilometers: number, suburbIncreaseProcent: number, vsevolozhsk: number, pricePerKilometerAdd: Array<object>, pricePerKilometerDel: Array<object>}) => {
        return await axios.put(`${this.host}/adminOptions/price/man/update`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    getTimeForCar = async () => {
        return await axios.get(`${this.host}/adminOptions/time/car/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    changeTimeForCar = async (data: {deadlineDeliveryMin: number, deliveryPeriodForAllMin: number, orderReadinessDuring: number, orderReadinessFrom: number, orderReadinessTo: number, orderTodayDelivery: number, orderTodayLeave: number, orderTomorrowDelivery: number, orderTomorrowLeave: number, samplingInterval: number, standardDeliveryPeriod: number, standardDeliveryPeriodFromStart: number}) => {
        return await axios.put(`${this.host}/adminOptions/time/car/update`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
    getTimeForMan = async () => {
        return await axios.get(`${this.host}/adminOptions/time/man/get`, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8',
            }
        }).then((res) => {
            return res.data
        });
    }
    changeTimeForMan = async (data: { crossingNeva: number, increaseProcent: number, exactTimeEnable: boolean, exactTimePrice: number, outOfCAD: number, outOfCity: number, payBankCardCommissionEnable: boolean, payBankCardCommissionProcent: number, suburbIncreaseKilometers: number, suburbIncreaseProcent: number, vsevolozhsk: number, pricePerKilometerAdd: Array<object>, pricePerKilometerDel: Array<object>}) => {
        return await axios.put(`${this.host}/adminOptions/time/man/update`, data, {
            headers: {
                'token': token,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }).then(res => res.data);
    }
}