import { ActionType, createAction } from 'typesafe-actions';

export const paytype = createAction("@paytype/START")<any>();
export const paytypeSuccess = createAction("@paytype/SUCCESS")<any>();
export const paytypeError = createAction("@paytype/ERROR")<any>();


export type PayTypeActions = ActionType<typeof paytype> | ActionType<typeof paytypeSuccess> | ActionType<typeof paytypeError>;