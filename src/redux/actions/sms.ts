import { ActionType, createAction } from 'typesafe-actions';

export const sms = createAction("@sms/START")<any>();
export const smsSuccess = createAction("@sms/SUCCESS")<any>();
export const smsError = createAction("@sms/ERROR")<any>();


export type SmsActions = ActionType<typeof sms> | ActionType<typeof smsSuccess> | ActionType<typeof smsError>;