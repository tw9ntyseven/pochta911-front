import { ActionType, createAction } from 'typesafe-actions';

export const termofpayment = createAction("@termofpayment/START")<any>();
export const termofpaymentSuccess = createAction("@termofpayment/SUCCESS")<any>();
export const termofpaymentError = createAction("@termofpayment/ERROR")<any>();


export type TermOfPaymentActions = ActionType<typeof termofpayment> | ActionType<typeof termofpaymentSuccess> | ActionType<typeof termofpaymentError>;