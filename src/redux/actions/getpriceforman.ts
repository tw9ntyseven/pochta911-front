import { ActionType, createAction } from 'typesafe-actions';

export const getpriceforman = createAction("@getpriceforman/START")<any>();
export const getpriceformanSuccess = createAction("@getpriceforman/SUCCESS")<any>();
export const getpriceformanError = createAction("@getpriceforman/ERROR")<any>();

export const changepriceforman = createAction("@getpriceforman/CHANGE/START")<any>();
export const changepriceformanSuccess = createAction("@getpriceforman/CHANGE/SUCCESS")<any>();
export const changepriceformanError = createAction("@getpriceforman/CHANGE/ERROR")<any>();


export type GetPriceForManActions = ActionType<typeof getpriceforman> | ActionType<typeof getpriceformanSuccess> | ActionType<typeof getpriceformanError> | ActionType<typeof changepriceforman> | ActionType<typeof changepriceformanSuccess> | ActionType<typeof changepriceformanError>;