import { ActionType, createAction } from 'typesafe-actions';

export const user = createAction("@user/START")<any>();
export const userSuccess = createAction("@user/SUCCESS")<any>();
export const userError = createAction("@user/ERROR")<any>();

export const changeUser = createAction("@user/CHANGE/START")<any>();
export const changeUserSuccess = createAction("@user/CHANGE/SUCCESS")<any>();
export const changeUserError = createAction("@user/CHANGE/ERROR")<any>();



export type UserActions = ActionType<typeof user> | ActionType<typeof userSuccess> | ActionType<typeof userError> | ActionType<typeof changeUser> | ActionType<typeof changeUserError> | ActionType<typeof changeUserSuccess>;