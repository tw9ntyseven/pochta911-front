import { ActionType, createAction } from 'typesafe-actions';

export const usertype = createAction("@usertype/START")<any>();
export const userTypeSuccess = createAction("@usertype/SUCCESS")<any>();
export const userTypeError = createAction("@usertype/ERROR")<any>();


export type UserTypeActions = ActionType<typeof usertype> | ActionType<typeof userTypeSuccess> | ActionType<typeof userTypeError>;