import { ActionType, createAction } from 'typesafe-actions';

export const auth = createAction("@auth/START")<any>();
export const authSuccess = createAction("@auth/SUCCESS")<any>();
export const authError = createAction("@auth/ERROR")<any>();


export type AuthActions = ActionType<typeof auth> | ActionType<typeof authSuccess> | ActionType<typeof authError>;