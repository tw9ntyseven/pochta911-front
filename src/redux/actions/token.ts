import { ActionType, createAction } from "typesafe-actions";


export const refreshToken = createAction("@token/REFRESH/START")();
export const refreshTokenSuccess = createAction("@token/REFRESH/SUCCESS")();
export const refreshTokenError = createAction("@token/REFRESH/ERROR")();


export type TokenActions = ActionType<typeof refreshToken> | ActionType<typeof refreshTokenError> | ActionType<typeof refreshTokenSuccess>;