import { ActionType, createAction } from 'typesafe-actions';

export const reg = createAction("@reg/START")<any>();
export const regSuccess = createAction("@reg/SUCCESS")<any>();
export const regError = createAction("@reg/ERROR")<any>();


export type RegActions = ActionType<typeof reg> | ActionType<typeof regSuccess> | ActionType<typeof regError>;