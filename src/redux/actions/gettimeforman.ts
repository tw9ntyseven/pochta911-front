import { ActionType, createAction } from 'typesafe-actions';

export const gettimeforman = createAction("@gettimeforman/START")<any>();
export const gettimeformanSuccess = createAction("@gettimeforman/SUCCESS")<any>();
export const gettimeformanError = createAction("@gettimeforman/ERROR")<any>();

export const changetimeforman = createAction("@gettimeforman/CHANGE/START")<any>();
export const changetimeformanSuccess = createAction("@gettimeforman/CHANGE/SUCCESS")<any>();
export const changetimeformanError = createAction("@gettimeforman/CHANGE/ERROR")<any>();


export type GetTimeForManActions = ActionType<typeof gettimeforman> | ActionType<typeof gettimeformanSuccess> | ActionType<typeof gettimeformanError> | ActionType<typeof changetimeforman> | ActionType<typeof changetimeformanSuccess> | ActionType<typeof changetimeformanError>;