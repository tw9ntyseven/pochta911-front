import { ActionType, createAction } from 'typesafe-actions';

export const getpriceforcar = createAction("@getpriceforcar/START")<any>();
export const getpriceforcarSuccess = createAction("@getpriceforcar/SUCCESS")<any>();
export const getpriceforcarError = createAction("@getpriceforcar/ERROR")<any>();

export const changepriceforcar = createAction("@getpriceforcar/CHANGE/START")<any>();
export const changepriceforcarSuccess = createAction("@getpriceforcar/CHANGE/SUCCESS")<any>();
export const changepriceforcarError = createAction("@getpriceforcar/CHANGE/ERROR")<any>();


export type GetPriceForCarActions = ActionType<typeof getpriceforcar> | ActionType<typeof getpriceforcarSuccess> | ActionType<typeof getpriceforcarError> | ActionType<typeof changepriceforcar> | ActionType<typeof changepriceforcarSuccess> | ActionType<typeof changepriceforcarError>;