import { ActionType, createAction } from 'typesafe-actions';

export const gettimeforcar = createAction("@gettimeforcar/START")<any>();
export const gettimeforcarSuccess = createAction("@gettimeforcar/SUCCESS")<any>();
export const gettimeforcarError = createAction("@gettimeforcar/ERROR")<any>();

export const changetimeforcar = createAction("@gettimeforcar/CHANGE/START")<any>();
export const changetimeforcarSuccess = createAction("@gettimeforcar/CHANGE/SUCCESS")<any>();
export const changetimeforcarError = createAction("@gettimeforcar/CHANGE/ERROR")<any>();


export type GetTimeForCarActions = ActionType<typeof gettimeforcar> | ActionType<typeof gettimeforcarSuccess> | ActionType<typeof gettimeforcarError> | ActionType<typeof changetimeforcar> | ActionType<typeof changetimeforcarSuccess> | ActionType<typeof changetimeforcarError>;