import { ActionType, createAction } from 'typesafe-actions';

export const addserv = createAction("@addserv/START")<any>();
export const addservSuccess = createAction("@addserv/SUCCESS")<any>();
export const addservError = createAction("@addserv/ERROR")<any>();


export type AddServActions = ActionType<typeof addserv> | ActionType<typeof addservSuccess> | ActionType<typeof addservError>