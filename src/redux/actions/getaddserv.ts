import { ActionType, createAction } from 'typesafe-actions';

export const getaddserv = createAction("@getaddserv/START")<any>();
export const getaddservSuccess = createAction("@getaddserv/SUCCESS")<any>();
export const getaddservError = createAction("@getaddserv/ERROR")<any>();

export const changeaddserv = createAction("@getaddserv/CHANGE/START")<any>();
export const changeaddservSuccess = createAction("@getaddserv/CHANGE/SUCCESS")<any>();
export const changeaddservError = createAction("@getaddserv/CHANGE/ERROR")<any>();

export const delserv = createAction("@getaddserv/DELETE/START")<any>();
export const delservSuccess = createAction("@getaddserv/DELETE/SUCCESS")<any>();
export const delservError = createAction("@getaddserv/DELETE/ERROR")<any>();



export type GetAddServActions = ActionType<typeof getaddserv> | ActionType<typeof getaddservSuccess> | ActionType<typeof getaddservError> | ActionType<typeof changeaddserv> | ActionType<typeof changeaddservSuccess> | ActionType<typeof changeaddservError> | ActionType<typeof delserv> | ActionType<typeof delservSuccess> | ActionType<typeof delservError>;