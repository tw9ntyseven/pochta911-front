import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { paytype, paytypeSuccess, paytypeError } from '../actions/paytype';
import { Action } from "../redux";
import { persistReducer } from "redux-persist";
import { GetPayTypeState } from "../types/paytype";

const defaultPayTypeState = { fetchStatus: FetchStatus.FETCHING, error: null, payTypeData: null };

export const reducer = (state: GetPayTypeState = defaultPayTypeState, action: Action) => {
  switch (action.type) {
    case getType(paytype):
        return {
            ...state,
            fetchStatus: state.payTypeData ? FetchStatus.FETCHED : FetchStatus.FETCHING
        };
    case getType(paytypeSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          payTypeData: action.payload
        }
    }
    case getType(paytypeError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const getpaytype: any = persistReducer({
    key: "paytype",
    storage: localStorage,
    whitelist: ["payTypeData"],
},reducer);
