import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { usertype, userTypeSuccess, userTypeError } from '../actions/usertype';
import { Action } from "../redux";
import { persistReducer } from "redux-persist";
import { GetUserTypeState } from "../types/usertype";

const defaultUserTypeState = { fetchStatus: FetchStatus.FETCHING, error: null, userTypeData: null };

export const reducer = (state: GetUserTypeState = defaultUserTypeState, action: Action) => {
  switch (action.type) {
    case getType(usertype):
        return {
            ...state,
            fetchStatus: state.userTypeData ? FetchStatus.FETCHED : FetchStatus.FETCHING
        };
    case getType(userTypeSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          userTypeData: action.payload
        }
    }
    case getType(userTypeError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const getusertype: any = persistReducer({
    key: "usertype",
    storage: localStorage,
    whitelist: ["userTypeData"],
},reducer);
