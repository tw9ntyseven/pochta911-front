import { FetchStatus } from "../../common/types";
import { persistReducer } from "redux-persist";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { GetPriceForManState } from "../types/getpriceforman";
import { getpriceforman, getpriceformanSuccess, getpriceformanError } from '../actions/getpriceforman';
import { Action } from "../redux";

const defaultGetPriceForManState = { fetchStatus: FetchStatus.FETCHED, error: null, priceForManData: null  };

export const reducer = (state: GetPriceForManState = defaultGetPriceForManState, action: Action) => {
  switch (action.type) {
    case getType(getpriceforman):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(getpriceformanSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          priceForManData: action.payload
        }
    }
    case getType(getpriceformanError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};


export const getpriceformans: any = persistReducer({
  key: "getpriceforman",
  storage: localStorage,
  whitelist: ["priceForManData"]
}, reducer)