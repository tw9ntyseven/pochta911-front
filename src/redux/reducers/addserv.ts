import { FetchStatus } from "../../common/types";
import { persistReducer } from "redux-persist";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { AddServState } from "../types/addserv";
import { addserv, addservSuccess, addservError } from '../actions/addserv';
import { Action } from "../redux";

const defaultAddServState = { fetchStatus: FetchStatus.FETCHED, error: null };

export const reducer = (state: AddServState = defaultAddServState, action: Action) => {
  switch (action.type) {
    case getType(addserv):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(addservSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
        }
    }
    case getType(addservError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};


export const addservice: any = persistReducer({
  key: "addserv",
  storage: localStorage,
}, reducer)