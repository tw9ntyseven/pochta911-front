import { FetchStatus } from "../../common/types";
import { persistReducer } from "redux-persist";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { GetTimeForManState } from "../types/gettimeforman";
import { gettimeforman, gettimeformanSuccess, gettimeformanError } from '../actions/gettimeforman';
import { Action } from "../redux";

const defaultGetTimeForManState = { fetchStatus: FetchStatus.FETCHED, error: null, timeForManData: null  };

export const reducer = (state: GetTimeForManState = defaultGetTimeForManState, action: Action) => {
  switch (action.type) {
    case getType(gettimeforman):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(gettimeformanSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          timeForManData: action.payload
        }
    }
    case getType(gettimeformanError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};


export const gettimeformans: any = persistReducer({
  key: "gettimeforman",
  storage: localStorage,
  whitelist: ["timeForManData"]
}, reducer)