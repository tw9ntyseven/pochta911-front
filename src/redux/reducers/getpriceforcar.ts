import { FetchStatus } from "../../common/types";
import { persistReducer } from "redux-persist";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { GetPriceForCarState } from "../types/getpriceforcar";
import { getpriceforcar, getpriceforcarSuccess, getpriceforcarError } from '../actions/getpriceforcar';
import { Action } from "../redux";

const defaultGetPriceForCarState = { fetchStatus: FetchStatus.FETCHED, error: null, priceForCarData: null  };

export const reducer = (state: GetPriceForCarState = defaultGetPriceForCarState, action: Action) => {
  switch (action.type) {
    case getType(getpriceforcar):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(getpriceforcarSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          priceForCarData: action.payload
        }
    }
    case getType(getpriceforcarError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};


export const getpriceforcars: any = persistReducer({
  key: "getpriceforcar",
  storage: localStorage,
  whitelist: ["priceForCarData"]
}, reducer)