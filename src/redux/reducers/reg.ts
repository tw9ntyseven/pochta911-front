import { FetchStatus } from "../../common/types";
import { persistReducer } from "redux-persist";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { RegistrationState } from "../types/reg";
import { reg, regSuccess, regError } from '../actions/reg';
import { Action } from "../redux";

const defaultRegState = { fetchStatus: FetchStatus.FETCHED, error: null, user: null };

export const reducer = (state: RegistrationState = defaultRegState, action: Action) => {
  switch (action.type) {
    case getType(reg):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(regSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          user: action.payload
        }
    }
    case getType(regError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};


export const registration: any = persistReducer({
  key: "reg",
  storage: localStorage,
  whitelist: ["user"]
}, reducer)