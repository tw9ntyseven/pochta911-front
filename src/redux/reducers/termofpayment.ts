import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { Action } from "../redux";
import { persistReducer } from "redux-persist";
import { termofpayment, termofpaymentError, termofpaymentSuccess } from "../actions/termofpayment";
import { GetTermOfPaymentState } from "../types/termofpayment";

const defaultTermOfPaymentState = { fetchStatus: FetchStatus.FETCHING, error: null, termOfPaymentData: null };

export const reducer = (state: GetTermOfPaymentState = defaultTermOfPaymentState, action: Action) => {
  switch (action.type) {
    case getType(termofpayment):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(termofpaymentSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          termOfPaymentData: action.payload
        }
    }
    case getType(termofpaymentError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const gettermofpayment: any = persistReducer({
    key: "termofpayment",
    storage: localStorage,
    whitelist: ["termOfPaymentData"],
},reducer);
