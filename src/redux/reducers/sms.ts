import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { sms, smsSuccess, smsError } from '../actions/sms';
import { Action } from "../redux";
import { persistReducer } from "redux-persist";
import { SmsWindowState } from "../types/sms";

const defaultSmsState = { fetchStatus: FetchStatus.FETCHED, error: null, isReg: false };

export const reducer = (state: SmsWindowState = defaultSmsState, action: Action) => {
  switch (action.type) {
    case getType(sms):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(smsSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          isReg: true
        }
    }
    case getType(smsError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const smsWindow: any = persistReducer({
    key: "sms",
    storage: localStorage,
},reducer);
