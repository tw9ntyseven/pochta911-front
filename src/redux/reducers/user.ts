import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { user, userSuccess, userError } from '../actions/user';
import { Action } from "../redux";
import { persistReducer } from "redux-persist";
import { GetUserState } from "../types/user";

const defaultUserState = { fetchStatus: FetchStatus.FETCHING, error: null, userData: null };

export const reducer = (state: GetUserState = defaultUserState, action: Action) => {
  switch (action.type) {
    case getType(user):
        return {
            ...state,
            fetchStatus: state.userData ? FetchStatus.FETCHED : FetchStatus.FETCHING
        };
    case getType(userSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          userData: action.payload
        }
    }
    case getType(userError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const getuser: any = persistReducer({
    key: "user",
    storage: localStorage,
    whitelist: ["userData"],
},reducer);
