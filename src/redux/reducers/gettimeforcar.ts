import { FetchStatus } from "../../common/types";
import { persistReducer } from "redux-persist";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { GetTimeForCarState } from "../types/gettimeforcar";
import { gettimeforcar, gettimeforcarSuccess, gettimeforcarError } from '../actions/gettimeforcar';
import { Action } from "../redux";

const defaultGetTimeForCarState = { fetchStatus: FetchStatus.FETCHED, error: null, timeForCarData: null  };

export const reducer = (state: GetTimeForCarState = defaultGetTimeForCarState, action: Action) => {
  switch (action.type) {
    case getType(gettimeforcar):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(gettimeforcarSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          timeForCarData: action.payload
        }
    }
    case getType(gettimeforcarError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};


export const gettimeforcars: any = persistReducer({
  key: "gettimeforcar",
  storage: localStorage,
  whitelist: ["timeForCarData"]
}, reducer)