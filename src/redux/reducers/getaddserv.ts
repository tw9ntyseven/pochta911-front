import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { getaddserv, getaddservSuccess, getaddservError } from '../actions/getaddserv';
import { Action } from "../redux";
import { persistReducer } from "redux-persist";
import { GetAddServState } from "../types/getaddserv";

const defaultGetAddServState = { fetchStatus: FetchStatus.FETCHING, error: null, getAddServData: null };

export const reducer = (state: GetAddServState = defaultGetAddServState, action: Action) => {
  switch (action.type) {
    case getType(getaddserv):
        return {
            ...state,
            fetchStatus: state.getAddServData ? FetchStatus.FETCHED : FetchStatus.FETCHING
        };
    case getType(getaddservSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          getAddServData: action.payload
        }
    }
    case getType(getaddservError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const getaddservice: any = persistReducer({
    key: "getaddserv",
    storage: localStorage,
    whitelist: ["getAddServData"],
},reducer);
