import { connectRouter } from "connected-react-router";
import { CombinedState, combineReducers } from "redux";
import { registration } from "./reg";
import { authorization } from "./auth";
import { smsWindow } from "./sms"
import { getuser } from "./user";
import { getusertype } from './usertype';
import { gettermofpayment } from "./termofpayment";
import { getaddservice } from "./getaddserv";
import { addservice } from "./addserv";
import { getpriceforcars } from "./getpriceforcar";
import { getpriceformans } from "./getpriceforman";
import { gettimeforcars } from "./gettimeforcar"
import { gettimeformans } from "./gettimeforman";
import { createBrowserHistory } from "history";
import { State } from "../redux";
import { getpaytype } from "./paytype";


export const history = createBrowserHistory();

export const reducers = combineReducers<CombinedState<State>>({
    registration,
    authorization,
    getuser,
    getusertype,
    getpaytype,
    getaddservice,
    getpriceforcars,
    getpriceformans,
    gettimeforcars,
    gettimeformans,
    addservice,
    smsWindow,
    gettermofpayment,
    router: connectRouter(history),
});