import { FetchStatus } from "../../common/types";
import localStorage from "redux-persist/es/storage";
import { getType } from "typesafe-actions";
import { auth, authSuccess, authError } from '../actions/auth';
import { Action } from "../redux";
import { AuthorizationState } from "../types/auth";
import { persistReducer } from "redux-persist";

const defaultAuthState = { fetchStatus: FetchStatus.FETCHED, error: null, isAuth: false };

export const reducer = (state: AuthorizationState = defaultAuthState, action: Action) => {
  switch (action.type) {
    case getType(auth):
        return {
            ...state,
            fetchStatus: FetchStatus.FETCHING
        };
    case getType(authSuccess): {
        return {
          ...state,
          fetchStatus: FetchStatus.FETCHED,
          isAuth: true
        }
    }
    case getType(authError): {
      return {
        ...state,
        fetchStatus: FetchStatus.ERROR,
        error: action.payload
      }
    }
    default:
        return state;
  }
};

export const authorization: any = persistReducer({
    key: "auth",
    storage: localStorage,
    whitelist: ["isAuth"],
},reducer);
