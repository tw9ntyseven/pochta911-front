import { Middleware, Dispatch, MiddlewareAPI, Action } from "redux";
import { getType } from "typesafe-actions";
import { reg } from "../actions/reg";
import { refreshToken, refreshTokenError, refreshTokenSuccess } from "../actions/token";
import { State } from "../redux";
import jwt from 'jsonwebtoken';
import moment from 'moment';
import { auth, authError, authSuccess } from "../actions/auth";

export const authMiddleware: Middleware<State, State, Dispatch<Action>> = (
    store: MiddlewareAPI<Dispatch<Action>>
  ): ((next: Dispatch<Action>) => (action: Action) => Action) => (
    next: Dispatch<Action>
  ): ((action: Action) => Action) => (action: Action): Action => {
    if (action?.type) {
        if (action.type !== getType(reg) && action.type !== getType(refreshToken) && action.type !== getType(refreshTokenSuccess) && action.type !== getType(refreshTokenError) && action.type !== getType(auth) && action.type !== getType(authSuccess) && action.type !== getType(authError) && action.type !== "@@router/LOCATION_CHANGE") {
            const token = localStorage.getItem("AccessToken");
            const refresh = localStorage.getItem("RefreshToken");

            if (refresh) {
              if (token) {
                let decoded: any = jwt.decode(token);
                if (decoded.exp < moment().unix()) {
                    store.dispatch(refreshToken())
                }
              } else {
                store.dispatch(refreshToken())
              }
            } else {
              store.dispatch({
                type: '@@router/LOCATION_CHANGE',
                payload: {
                  location: {
                    pathname: '/registration',
                  },
                  action: 'POP',
                  isFirstRendering: true
                }
              })
            }
        }
    }
    return next(action);
  };