import { applyMiddleware, createStore } from "redux";
import { combineEpics, createEpicMiddleware, EpicMiddleware } from "redux-observable";
import { RouterState } from 'connected-react-router';
import { persistStore } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import { RegActions } from "./actions/reg";
import { AuthDataProvider } from "../api/AuthDataProvider";
import { regEpics } from "./epics/reg";
import { RegistrationState } from "./types/reg";
import { reducers } from "./reducers";
import { TokenActions } from './actions/token';
import { authMiddleware } from "./middleware/auth";
import { tokenEpics } from "./epics/token";
import { authEpics } from "./epics/auth";
import { AuthActions } from "./actions/auth";
import { AuthorizationState } from "./types/auth";
import { SmsActions } from "./actions/sms";
import { SmsWindowState } from "./types/sms";
import { smsEpics } from "./epics/sms";
import { UserActions } from "./actions/user";
import { userEpics } from "./epics/user";
import { GetUserState } from "./types/user";
import { UserTypeActions } from "./actions/usertype";
import { GetUserTypeState } from "./types/usertype";
import { userTypeEpics } from "./epics/usertype";
import { TermOfPaymentActions } from "./actions/termofpayment";
import { termOfPaymentEpics } from "./epics/termofpayment";
import { GetTermOfPaymentState } from "./types/termofpayment";
import { PayTypeActions } from "./actions/paytype";
import { payTypeEpics } from "./epics/paytype";
import { GetPayTypeState } from "./types/paytype";
import { getAddServEpics } from "./epics/getaddserv";
import { GetAddServActions } from "./actions/getaddserv";
import { GetAddServState } from "./types/getaddserv";
import { AddServActions } from "./actions/addserv";
import { getServEpics } from "./epics/addserv";
import { AddServState } from "./types/addserv";
import { GetPriceForCarActions } from "./actions/getpriceforcar";
import { getPriceForCarEpics } from "./epics/getpriceforcar";
import { GetPriceForCarState } from "./types/getpriceforcar";
import { GetPriceForManActions } from "./actions/getpriceforman";
import { getPriceForManEpics } from "./epics/getpriceforman";
import { GetPriceForManState } from "./types/getpriceforman";
import { GetTimeForCarActions } from "./actions/gettimeforcar";
import { getTimeForCarEpics } from "./epics/gettimeforcar";
import { GetTimeForCarState } from "./types/gettimeforcar";
import { GetTimeForManActions } from "./actions/gettimeforman";
import { getTimeForManEpics } from "./epics/gettimeforman";
import { GetTimeForManState } from "./types/gettimeforman";

const host = "http://185.119.57.32/api";

export type Action = RegActions | TokenActions | AuthActions | SmsActions | UserActions | UserTypeActions | TermOfPaymentActions | PayTypeActions | GetAddServActions | AddServActions | GetPriceForCarActions | GetPriceForManActions | GetTimeForCarActions | GetTimeForManActions;

export interface EpicDeps {
    authDataProvider: AuthDataProvider;
}

export interface State {
    router: RouterState;
    registration: RegistrationState;
    authorization: AuthorizationState;
    getuser: GetUserState;
    getusertype: GetUserTypeState;
    gettermofpayment: GetTermOfPaymentState;
    getpaytype: GetPayTypeState;
    getpriceforcars: GetPriceForCarState;
    getpriceformans: GetPriceForManState;
    gettimeforcars: GetTimeForCarState;
    gettimeformans: GetTimeForManState;
    getaddservice: GetAddServState;
    addservice: AddServState;
    smsWindow: SmsWindowState;
} 

const createMiddleware = (
    epicMiddleware: EpicMiddleware<Action,Action, State, EpicDeps>
) => applyMiddleware(epicMiddleware, authMiddleware);

const composeEnhancers = composeWithDevTools({ serialize: true });

const epicMiddleware = createEpicMiddleware<Action, Action, State, EpicDeps>({
    dependencies: {
        authDataProvider: new AuthDataProvider(host),
    }
});

export const store = createStore<State, Action, {}, {}>(
    reducers,
    composeEnhancers(createMiddleware(epicMiddleware))
)

export const persistor = persistStore(store);

epicMiddleware.run(combineEpics(regEpics, tokenEpics,userEpics, authEpics, smsEpics, userTypeEpics, termOfPaymentEpics, payTypeEpics, getAddServEpics, getServEpics, getPriceForCarEpics, getPriceForManEpics, getTimeForCarEpics, getTimeForManEpics));



