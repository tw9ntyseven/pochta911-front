import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { paytype, paytypeError, paytypeSuccess } from "../actions/paytype";

const payTypeEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(paytype)),
    switchMap(() => {
      return from(deps.authDataProvider.getpaytype()).pipe(
        switchMap((response) => {
          return of(paytypeSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(paytypeError(e.response.data.message));
          } else {
            return of(paytypeError(e));
          }
        })
      );
    })
  );
};

export const payTypeEpics = combineEpics(payTypeEpic);