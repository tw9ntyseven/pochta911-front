import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { changetimeforman, changetimeformanError, changetimeformanSuccess, gettimeforman, gettimeformanError, gettimeformanSuccess } from "../actions/gettimeforman";

const getTimeForManEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(gettimeforman)),
    switchMap(() => {
      return from(deps.authDataProvider.getTimeForMan()).pipe(
        switchMap((response) => {
          return of(gettimeformanSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(gettimeformanError(e.response.data.message));
          } else {
            return of(gettimeformanError(e));
          }
        })
      );
    })
  );
};

const changeTimeForManEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(changetimeforman)),
      switchMap(({ payload }: ActionType<typeof gettimeforman>) => {
        return from(deps.authDataProvider.changeTimeForMan(payload)).pipe(
          switchMap((response) => {
            return of(changetimeformanSuccess(response), gettimeforman({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(changetimeformanError(e.response.data.message));
            } else {
              return of(changetimeformanError(e));
            }
          })
        );
      })
    );
  };

export const getTimeForManEpics = combineEpics(getTimeForManEpic, changeTimeForManEpic);