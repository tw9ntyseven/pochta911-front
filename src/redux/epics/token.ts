import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { refreshToken, refreshTokenSuccess, refreshTokenError } from '../actions/token';
import localStorage from "redux-persist/es/storage";

const tokenEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(refreshToken)),
      switchMap(() => {
        return from(deps.authDataProvider.refreshToken()).pipe(
            switchMap((response) => {
                localStorage.setItem("AccessToken", response.access)
                localStorage.setItem("RefreshToken", response.refresh)
                return of(refreshTokenSuccess());
            }),
            catchError(e => {
                return of(refreshTokenError());
            })
        );
      })
    );
  };
  
  export const tokenEpics = combineEpics(tokenEpic);