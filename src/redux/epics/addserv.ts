import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { ActionType, getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { addserv, addservSuccess, addservError } from "../actions/addserv";
import { getaddserv } from "../actions/getaddserv";


  const addServEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(addserv)),
      switchMap(({ payload }: ActionType<typeof addserv>) => {
        return from(deps.authDataProvider.addServ(payload)).pipe(
          switchMap((response) => {
            return of(addservSuccess(response), getaddserv({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(addservError(e.response.data.message));
            } else {
              return of(addservError(e));
            }
          })
        );
      })
    );
  };

export const getServEpics = combineEpics(addServEpic);