import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { auth, authError, authSuccess } from "../actions/auth";

const authEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(auth)),
    switchMap(({ payload }: ActionType<typeof auth>) => {
      return from(deps.authDataProvider.authorization(payload)).pipe(
        switchMap((response) => {
            localStorage.setItem("AccessToken", response.tokens.access)
            localStorage.setItem("RefreshToken", response.tokens.refresh)
          return of(authSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(authError(e.response.data.message));
          } else {
            return of(authError(e.message));
          }
        })
      );
    })
  );
};

export const authEpics = combineEpics(authEpic);