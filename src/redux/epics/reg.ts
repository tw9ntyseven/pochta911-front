import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { reg, regError, regSuccess } from "../actions/reg";

const regEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(reg)),
    switchMap(({ payload }: ActionType<typeof reg>) => {
      return from(deps.authDataProvider.register(payload)).pipe(
        switchMap((response) => {
          return of(regSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(regError(e.response.data.message));
          } else {
            return of(regError(e));
          }
        })
      );
    })
  );
};

export const regEpics = combineEpics(regEpic);