import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { usertype, userTypeError, userTypeSuccess } from "../actions/usertype";

const userTypeEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(usertype)),
    switchMap(() => {
      return from(deps.authDataProvider.getusertype()).pipe(
        switchMap((response) => {
          return of(userTypeSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(userTypeError(e.response.data.message));
          } else {
            return of(userTypeError(e));
          }
        })
      );
    })
  );
};

export const userTypeEpics = combineEpics(userTypeEpic);