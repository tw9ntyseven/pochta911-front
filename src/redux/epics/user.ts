import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { ActionType, getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { changeUser, changeUserError, changeUserSuccess, user, userError, userSuccess } from "../actions/user";

const getUserEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(user)),
    switchMap(() => {
      return from(deps.authDataProvider.getuser()).pipe(
        switchMap((response) => {
          return of(userSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(userError(e.response.data.message));
          } else {
            return of(userError(e));
          }
        })
      );
    })
  );
};

const changeUserEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(changeUser)),
    switchMap(({ payload }: ActionType<typeof changeUser>) => {
      return from(deps.authDataProvider.changeUser(payload)).pipe(
        switchMap((response) => {
          return of(changeUserSuccess(response), user({}))
        }),
        catchError(e => {
          if (e.response) {
            return of(changeUserError(e.response.data.message));
          } else {
            return of(changeUserError(e));
          }
        })
      );
    })
  );
};

export const userEpics = combineEpics(getUserEpic, changeUserEpic);