import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { changepriceforman, changepriceformanError, changepriceformanSuccess, getpriceforman, getpriceformanError, getpriceformanSuccess } from "../actions/getpriceforman";

const getPriceForManEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(getpriceforman)),
    switchMap(() => {
      return from(deps.authDataProvider.getPriceForMan()).pipe(
        switchMap((response) => {
          return of(getpriceformanSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(getpriceformanError(e.response.data.message));
          } else {
            return of(getpriceformanError(e));
          }
        })
      );
    })
  );
};

const changePriceForManEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(changepriceforman)),
      switchMap(({ payload }: ActionType<typeof getpriceforman>) => {
        return from(deps.authDataProvider.changePriceForMan(payload)).pipe(
          switchMap((response) => {
            return of(changepriceformanSuccess(response), getpriceforman({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(changepriceformanError(e.response.data.message));
            } else {
              return of(changepriceformanError(e));
            }
          })
        );
      })
    );
  };

export const getPriceForManEpics = combineEpics(getPriceForManEpic, changePriceForManEpic);