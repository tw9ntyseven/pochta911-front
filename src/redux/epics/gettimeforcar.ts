import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { changetimeforcar, changetimeforcarError, changetimeforcarSuccess, gettimeforcar, gettimeforcarError, gettimeforcarSuccess } from "../actions/gettimeforcar";

const getTimeForCarEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(gettimeforcar)),
    switchMap(() => {
      return from(deps.authDataProvider.getTimeForCar()).pipe(
        switchMap((response) => {
          return of(gettimeforcarSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(gettimeforcarError(e.response.data.message));
          } else {
            return of(gettimeforcarError(e));
          }
        })
      );
    })
  );
};

const changeTimeForCarEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(changetimeforcar)),
      switchMap(({ payload }: ActionType<typeof gettimeforcar>) => {
        return from(deps.authDataProvider.changeTimeForCar(payload)).pipe(
          switchMap((response) => {
            return of(changetimeforcarSuccess(response), gettimeforcar({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(changetimeforcarError(e.response.data.message));
            } else {
              return of(changetimeforcarError(e));
            }
          })
        );
      })
    );
  };

export const getTimeForCarEpics = combineEpics(getTimeForCarEpic, changeTimeForCarEpic);