import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { termofpayment, termofpaymentError, termofpaymentSuccess } from "../actions/termofpayment";

const termOfPaymentEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(termofpayment)),
    switchMap(() => {
      return from(deps.authDataProvider.gettermofpayment()).pipe(
        switchMap((response) => {
          return of(termofpaymentSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(termofpaymentError(e.response.data.message));
          } else {
            return of(termofpaymentError(e));
          }
        })
      );
    })
  );
};

export const termOfPaymentEpics = combineEpics(termOfPaymentEpic);