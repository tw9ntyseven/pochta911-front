import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { changepriceforcar, changepriceforcarError, changepriceforcarSuccess, getpriceforcar, getpriceforcarError, getpriceforcarSuccess } from "../actions/getpriceforcar";

const getPriceForCarEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(getpriceforcar)),
    switchMap(() => {
      return from(deps.authDataProvider.getPriceForCar()).pipe(
        switchMap((response) => {
          return of(getpriceforcarSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(getpriceforcarError(e.response.data.message));
          } else {
            return of(getpriceforcarError(e));
          }
        })
      );
    })
  );
};

const changePriceForCarEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(changepriceforcar)),
      switchMap(({ payload }: ActionType<typeof getpriceforcar>) => {
        return from(deps.authDataProvider.changePriceForCar(payload)).pipe(
          switchMap((response) => {
            return of(changepriceforcarSuccess(response), getpriceforcar({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(changepriceforcarError(e.response.data.message));
            } else {
              return of(changepriceforcarError(e));
            }
          })
        );
      })
    );
  };

export const getPriceForCarEpics = combineEpics(getPriceForCarEpic, changePriceForCarEpic);