import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { ActionType, getType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { getaddserv, getaddservError, getaddservSuccess, changeaddserv, changeaddservSuccess, changeaddservError, delserv, delservSuccess, delservError } from "../actions/getaddserv";

const getAddServEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(getaddserv)),
    switchMap(() => {
      return from(deps.authDataProvider.getaddservice()).pipe(
        switchMap((response) => {
          return of(getaddservSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(getaddservError(e.response.data.message));
          } else {
            return of(getaddservError(e));
          }
        })
      );
    })
  );
};

const changeAddServEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(changeaddserv)),
      switchMap(({ payload }: ActionType<typeof changeaddserv>) => {
        return from(deps.authDataProvider.changeAddServ(payload)).pipe(
          switchMap((response) => {
            return of(changeaddservSuccess(response), getaddserv({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(changeaddservError(e.response.data.message));
            } else {
              return of(changeaddservError(e));
            }
          })
        );
      })
    );
  };

  const delServEpic: FuncEpic = (action$: any, store$, deps) => {
    return action$.pipe(
      ofType(getType(delserv)),
      switchMap(({ payload }: ActionType<typeof delserv>) => {
        return from(deps.authDataProvider.delServ(payload)).pipe(
          switchMap((response) => {
            return of(delservSuccess(response), getaddserv({}))
          }),
          catchError(e => {
            if (e.response) {
              return of(delservError(e.response.data.message));
            } else {
              return of(delservError(e));
            }
          })
        );
      })
    );
  };


export const getAddServEpics = combineEpics(getAddServEpic, changeAddServEpic, delServEpic);