import { combineEpics, ofType } from "redux-observable";
import { from, of } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { getType, ActionType } from "typesafe-actions";
import { FuncEpic } from "../../common/types";
import { sms, smsError, smsSuccess } from "../actions/sms";

const smsEpic: FuncEpic = (action$: any, store$, deps) => {
  return action$.pipe(
    ofType(getType(sms)),
    switchMap(({ payload }: ActionType<typeof sms>) => {
      return from(deps.authDataProvider.smsWindow(payload)).pipe(
        switchMap((response) => {
          return of(smsSuccess(response))
        }),
        catchError(e => {
          if (e.response) {
            return of(smsError(e.response.data.message));
          } else {
            return of(smsError(e));
          }
        })
      );
    })
  );
};

export const smsEpics = combineEpics(smsEpic);