import { FetchStatus } from "../../common/types";

export interface GetUserTypeState {
    fetchStatus: FetchStatus;
    userTypeData: any;
    error: any;
}