import { FetchStatus } from "../../common/types";

export interface SmsWindowState {
    fetchStatus: FetchStatus;
    isReg: any,
    error: any;
}