import { FetchStatus } from "../../common/types";

export interface AddServState {
    fetchStatus: FetchStatus;
    error: any;
}