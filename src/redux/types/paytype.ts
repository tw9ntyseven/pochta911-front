import { FetchStatus } from "../../common/types";

export interface GetPayTypeState {
    fetchStatus: FetchStatus;
    payTypeData: any;
    error: any;
}