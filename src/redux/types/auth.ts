import { FetchStatus } from "../../common/types";

export interface AuthorizationState {
    fetchStatus: FetchStatus;
    isAuth: any,
    error: any;
}