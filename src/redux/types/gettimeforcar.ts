import { FetchStatus } from "../../common/types";

export interface GetTimeForCarState {
    fetchStatus: FetchStatus;
    timeForCarData: any,
    error: any;
}