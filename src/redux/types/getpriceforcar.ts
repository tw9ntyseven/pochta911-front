import { FetchStatus } from "../../common/types";

export interface GetPriceForCarState {
    fetchStatus: FetchStatus;
    priceForCarData: any,
    error: any;
}