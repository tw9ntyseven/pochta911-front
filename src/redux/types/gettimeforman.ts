import { FetchStatus } from "../../common/types";

export interface GetTimeForManState {
    fetchStatus: FetchStatus;
    timeForManData: any,
    error: any;
}