import { FetchStatus } from "../../common/types";

export interface GetAddServState {
    fetchStatus: FetchStatus;
    getAddServData: any;
    error: any;
}