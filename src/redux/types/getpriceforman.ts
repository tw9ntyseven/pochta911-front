import { FetchStatus } from "../../common/types";

export interface GetPriceForManState {
    fetchStatus: FetchStatus;
    priceForManData: any,
    error: any;
}