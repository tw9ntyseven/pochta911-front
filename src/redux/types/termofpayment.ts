import { FetchStatus } from "../../common/types";

export interface GetTermOfPaymentState {
    fetchStatus: FetchStatus;
    termOfPaymentData: any;
    error: any;
}