import { FetchStatus } from "../../common/types";

export interface GetUserState {
    fetchStatus: FetchStatus;
    userData: any;
    error: any;
}