import { FetchStatus } from "../../common/types";

export interface RegistrationState {
    fetchStatus: FetchStatus;
    error: any;
    user: any;
}