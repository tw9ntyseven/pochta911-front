import React, { useState } from 'react'
import './Home.css'
import DatePicker, { registerLocale } from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import ru from 'date-fns/locale/ru';
import { Link } from 'react-router-dom';
import { Table } from '../../Components/Table/Table';
import { ExportExcel } from '../../Components/ExportExcel/ExportExcel';
registerLocale('ru', ru)

export const Home : React.FC = () => {
    const [ dateValue, setDateValue ] = useState<any>(null);
    const [showExcel, setShowExcel] = useState<boolean>(false);
    return (
        <div className="wrapper">
            <div className="screen-margin home">
                <div className="container">
                    <div className="home__settings">
                        {/* <h1 className="home__logo"><i className="fas fa-bus-alt"></i> Логистика</h1> */}
                        <div className="home__settings-wrapper">
                            <Link to="/logistics/create-order" className="btn home__btn-new-order"><i className="fas fa-plus mar"></i> Новый заказ</Link>
                            <div className="home__inputs">
                                <span className="home__title">Дата</span>
                                <DatePicker
                                    locale="ru"
                                    className="ipt home__ipt-datepicker"
                                    selected={dateValue}
                                    onChange={date => setDateValue(date)}
                                    dateFormat='dd.MM.yyyy'
                                    />
                                <span className="home__title">Поиск</span>
                                <input type="text" className="ipt home__ipt-search"/>
                                <button onClick={e => setShowExcel(!showExcel)} className="btn home__btn-excel"><i className="fas fa-file-excel mar"></i> Выгрузить в Excel</button>
                                {showExcel ?
                                <ExportExcel onClick={e => setShowExcel(!showExcel)} />
                                : null}
                            </div>
                        </div>
                        <div className="home__filters">
                            <div className="home__statuses">
                            <span className="home__title">Статусы:</span>
                                <label style={{background: "#D2FCB5"}} className="home__statuses-item">
                                    <input type="checkbox" className="home__statuses-item-checkbox"/>
                                    новый
                                </label>
                                <label style={{background: "#FCF4B5"}} className="home__statuses-item">
                                    <input type="checkbox" className="home__statuses-item-checkbox"/>
                                    в исполнении
                                </label>
                                <label style={{background: "#A5E6FE"}} className="home__statuses-item">
                                    <input type="checkbox" className="home__statuses-item-checkbox"/>
                                    выполнен
                                </label>
                                <label style={{background: "#FEA5B8"}} className="home__statuses-item">
                                    <input type="checkbox" className="home__statuses-item-checkbox"/>
                                    отменен
                                </label>
                            </div>
                            <div className="home__filter">
                                <span className="home__title">Фильтр</span>
                                <input type="text" className="ipt home__filter-ipt"/>
                            </div>
                        </div>
                    </div>
                    <div className="table">
                        <Table />
                    </div>
                </div>
            </div>
        </div>
    );
}