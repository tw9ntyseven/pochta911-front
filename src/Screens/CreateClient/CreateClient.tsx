import React, { useCallback, useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { FetchStatus } from '../../common/types';
import { Alert } from '../../Components/Alert/Alert';
import { ExtraContact } from '../../Components/ExtraContact/ExtraContact';
import { FavoriteAddress } from '../../Components/FavoriteAddress/FavoriteAddress';
import { PayType } from '../../Components/PayType/PayType';
import { TermsOfPayment  } from '../../Components/TermsOfPayment/TermsOfPayment';
import { UserType, UserTypeComponent } from '../../Components/UserType/UserType';
import { Wrapper } from '../../Components/Wrapper/Wrapper';
import { termofpayment } from '../../redux/actions/termofpayment';
import { changeUser, user } from '../../redux/actions/user';
import { State } from '../../redux/redux';
import { GetUserState } from '../../redux/types/user';
import './CreateClient.css'

export interface UserState extends GetUserState {
    user: (data: any) => {};
}

interface CreateClientComponentProps extends UserState {
    changeUser: (data: any) => {};
}

const CreateClientComponent : React.FC<CreateClientComponentProps> = ({ error, fetchStatus, user, userData, changeUser }) => {
    const [nameOwner, setNameOwner] = useState('');
    const [nameOfCompany, setNameOfCompany] = useState('');
    const [phoneInfo, setPhoneInfo] = useState('');
    const [phone, setPhone] = useState('');
    const [nameLegal, setNameLegal ] = useState('');
    const [addressLegal, setAddressLegal ] = useState('');
    const [agreementNum, setAgreementNum] = useState('');
    const [agreementDate, setAgreementDate ] = useState('');
    const [INN, setINN] = useState('');
    const [KPP, setKPP] = useState('');
    const [BIKBank, setBIKBank] = useState('');
    const [settlementAccount, setSettlementAccount] = useState('');
    const [taxType, setTaxType] = useState('');
    const [NDS, setNDS] = useState('');
    const [userTypeId, setUserTypeId] = useState('');
    const [payTypeId, setPayTypeId] = useState('');
    const [termsOfPaymentId, setTermsOfPaymentId] = useState('');
    const [emailFinancial, setEmailFinancial] = useState('');
    const [emailNotification, setEmailNotification] = useState('');
    const [favoriteAddressesDel, setFavoriteAddressesDel] = useState([]);
    const [favoriteAddressesAdd, setFavoriteAddressesAdd] = useState([]);
    const [additionalContactsDel, setAdditionalContactsDel] = useState([]);
    const [additionalContactsAdd, setAdditionalContactsAdd] = useState([]);

    const [errorMessage, setErrorMessage] = useState(false);
    const [message, setMessage] = useState('');


    const payTypeChangeCallback = useCallback(
        (e: any) => {
            setPayTypeId(e.target.value);
        },
        [payTypeId],
    )

    const changeUserTypeCallback = useCallback(
        (e: any) => {
            setUserTypeId(e.target.value);
        },
        [userTypeId],
    )

    const termsOfPaymentChangeCallback = useCallback(
        (e: any) => {
            setTermsOfPaymentId(e.target.value);
        },
        [termsOfPaymentId],
    )

    useEffect(() => {
        user({});
    }, []);

    const getValues = () => {
        let obj: any = {}
        if (nameOwner.length !== 0) obj.nameOwner = nameOwner;
        if (nameOfCompany.length !== 0) obj.nameOfCompany = nameOfCompany;
        if (phoneInfo.length !== 0) obj.phoneInfo = phoneInfo;
        if (phone.length !== 0) obj.phone = phone;
        if (nameLegal.length !== 0) obj.nameLegal = nameLegal;
        if (addressLegal.length !== 0) obj.addressLegal = addressLegal;
        if (agreementNum.length !== 0) obj.agreementNum = agreementNum;
        if (agreementDate.length !== 0) obj.agreementDate = agreementDate;
        if (INN.length !== 0) obj.INN = INN;
        if (KPP.length !== 0) obj.KPP = KPP;
        if (BIKBank.length !== 0) obj.BIKBank = BIKBank;
        if (settlementAccount.length !== 0) obj.settlementAccount = settlementAccount;
        if (taxType.length !== 0) obj.taxType = taxType;
        if (NDS.length !== 0) obj.NDS = NDS;
        if (userTypeId.length !== 0) obj.userTypeId = userTypeId;
        if (payTypeId.length !== 0) obj.payTypeId = payTypeId;
        if (termsOfPaymentId.length !== 0) obj.termsOfPaymentId = termsOfPaymentId;
        if (emailFinancial.length !== 0) obj.emailFinancial = emailFinancial;
        if (emailNotification.length !== 0) obj.emailNotification = emailNotification;
        if (favoriteAddressesAdd.length !== 0) obj.favoriteAddressesAdd = favoriteAddressesAdd;
        if (favoriteAddressesDel.length !== 0) obj.favoriteAddressesDel = favoriteAddressesDel;
        if (additionalContactsDel.length !== 0) obj.additionalContactsDel = additionalContactsDel;
        if (additionalContactsAdd.length !== 0) obj.additionalContactsAdd = additionalContactsAdd;

        return obj;
    }

    const saveUserPressHandler = () => {
        setErrorMessage(true);
        setMessage("Данные сохранены");
        changeUser({
            ...getValues()
        });
        console.log(payTypeId, "userTypeId");
        
    }
    console.log(userData, "USER");
    
    return (
        <>
        {errorMessage ?
            <Alert message={message} />
        :null}
        {fetchStatus === FetchStatus.ERROR ?
                <Alert message={error}/>
        : null}
        {
            fetchStatus === FetchStatus.FETCHING || !userData ?
            null
            :
            <Wrapper>
                <div className="create-cli">
                    <div className="flex">
                        <h1 className="home__logo">Карточка клиента:</h1>
                        <button onClick={e => saveUserPressHandler()} className="btn btn__right create-cour__btn">Сохранить</button>
                    </div>
                <div className="flex">
                    <div className="create-cour__ipts create-cli__ipts">
                        <div className="home__title create-order__title create-cour__title">Контактные данные / реквизиты</div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Тип клиента:</div>
                            <UserType userTypeId={userData?.data?.userTypeId} onChange={changeUserTypeCallback} />
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Название компании:</div>
                            <input onChange={e => setNameOfCompany(e.target.value)} defaultValue={userData?.data?.nameOfCompany} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Имя владельца аккаунта:</div>
                            <input onChange={e => setNameOwner(e.target.value)} defaultValue={userData?.data?.nameOwner} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Телефон:</div>
                            <input onChange={e => setPhone(e.target.value)} defaultValue={userData?.data?.phone} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Email (для оповещений о доставки):</div>
                            <input onChange={e => setEmailNotification(e.target.value)} defaultValue={userData?.data?.emailNotification} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Email (для финансовых отчетов, счетов):</div>
                            <input onChange={e => setEmailFinancial(e.target.value)} defaultValue={userData?.data?.emailFinancial} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '30px'}} className="flex">
                            <div className="home__title">Телефон для смс информирования:</div>
                            <input onChange={e => setPhoneInfo(e.target.value)} defaultValue={userData?.data?.phoneInfo} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>

                        {/* Данные */}
                        <div style={{marginTop: '30px'}} className="home__title create-order__title create-cour__title">Данные</div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Юридическое название:</div>
                            <input onChange={e => setNameLegal(e.target.value)} defaultValue={userData?.data?.nameLegal} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Юр. Адрес:</div>
                            <input onChange={e => setAddressLegal(e.target.value)} defaultValue={userData?.data?.addressLegal} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">№ Договора:</div>
                            <input onChange={e => setAgreementNum(e.target.value)} defaultValue={userData?.data?.agreementNum} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Дата договора:</div>
                            <input onChange={e => setAgreementDate(e.target.value)} defaultValue={userData?.data?.agreementDate} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">ИНН:</div>
                            <input onChange={e => setINN(e.target.value)} defaultValue={userData?.data?.INN} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">КПП:</div>
                            <input onChange={e => setKPP(e.target.value)} defaultValue={userData?.data?.KPP} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">БИК Банка:</div>
                            <input onChange={e => setBIKBank(e.target.value)} defaultValue={userData?.data?.BIKBank} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Расчетный счет:</div>
                            <input  onChange={e => setSettlementAccount(e.target.value)} defaultValue={userData?.data?.settlementAccount} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">Тип налогооблажения:</div>
                            <input onChange={e => setTaxType(e.target.value)} defaultValue={userData?.data?.taxType} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                        <div style={{marginBottom: '10px'}} className="flex">
                            <div className="home__title">НДС на товары принципала:</div>
                            <input onChange={e => setNDS(e.target.value)} defaultValue={userData?.data?.NDS} type="text" className="ipt btn__right create-cour__ipt"/>
                        </div>
                    </div>
                        
                    <div className="create-cli__cards">
                        <div className="create-cli__card">
                            <div className="home__title create-order__title create-cour__title">Дополнительные настройки</div>
                            <div style={{marginBottom: '10px'}} className="flex">
                                <div className="home__title">Тип оплаты:</div>
                                <PayType payTypeId={userData?.data?.payTypeId} value={payTypeId} onChange={payTypeChangeCallback} />
                            </div>
                            <div style={{marginBottom: '30px'}} className="flex">
                                <div className="home__title">Условия оплаты:</div>
                                <TermsOfPayment termOfPayment={userData?.data?.termsOfPaymentId} onChange={termsOfPaymentChangeCallback} />
                            </div>

                            {/* Любимые адреса */}
                            <div className="home__title create-order__title create-cour__title">Любимые адреса</div>
                            <FavoriteAddress save={() => saveUserPressHandler()} favoriteAddressesDel={favoriteAddressesDel} favoriteAddressesAdd={favoriteAddressesAdd} user={user} fetchStatus={fetchStatus} error={error} userData={userData} />

                            {/* Доп контакты */}
                            <div style={{marginTop: '30px'}} className="home__title create-order__title create-cour__title">Доп. контакты</div>
                            <ExtraContact save={() => saveUserPressHandler()} additionalContactsDel={additionalContactsDel} additionalContactsAdd={additionalContactsAdd} user={user} fetchStatus={fetchStatus} error={error} userData={userData} />
                            </div>
                        </div>
                    </div>
                </div>
            </Wrapper>
            }
        </>
    );
}
export const CreateClient = connect(
    ({ getuser }: State) => ({ ...getuser }),
    (dispatch) => {
        return {
            user: (data: any) => {
                return dispatch(user(data))
            },
            changeUser: (data: any) => {
                return dispatch(changeUser(data));
            }
        }
    }
)(CreateClientComponent)