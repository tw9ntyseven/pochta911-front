import React, { useEffect, useState } from 'react'
import './ServicesPanel.css'
import { Wrapper } from '../../Components/Wrapper/Wrapper'
import { ServicesAdd } from '../../Components/ServicesAdd/ServicesAdd';
import { GetAddServState } from '../../redux/types/getaddserv';
import { connect } from 'react-redux';
import { State } from '../../redux/redux';
import { changeaddserv, delserv, getaddserv } from '../../redux/actions/getaddserv';
import { FetchStatus } from '../../common/types';
import { Alert } from '../../Components/Alert/Alert';

interface AddServState extends GetAddServState {
    getaddserv: (data: any) => {};
}
interface DelServState {
    delserv: (data: any) => {};
}

interface ServicesPanelComponentsProps extends AddServState, DelServState {
    changeaddserv: (data: any) => {};
}

export const ServicesPanelComponents : React.FC<ServicesPanelComponentsProps> = ({ error, fetchStatus, getaddserv, getAddServData, changeaddserv, delserv }) => {
    const [showModal, setShowModal] = useState<boolean>(false);

    const [price, setPrice] = useState("");
    const [name, setName] = useState('');
    const [enable, setEnable] = useState<any>();
    const [identeficator, setIdenteficator] = useState('');

    const [errorMessage, setErrorMessage] = useState(false);
    const [message, setMessage] = useState('');

    useEffect(() => {
        getaddserv({});
    }, []);

    console.log(getAddServData, "DATA SERVICE");
    
    const getValues = () => {
        let priceNum = Number(price);
        let obj: any = {}
        if(identeficator.length !== 0) obj.id = identeficator;
        if(priceNum !== 0) obj.price = priceNum;
        if(name.length !== 0) obj.name = name;
        if(enable !== null) obj.enable = enable;

        return obj;
    }
    const notification = () => {
        setErrorMessage(true);
        setMessage("После введенных изменений не забудьте СОХРАНИТЬ!");
    }
    const saveAddServ = () => {
        changeaddserv({
            "additionalServices":[{
            ...getValues()
        }]})
        console.log(identeficator, "IDENTEFICATOR");   
    }
    const delServ = (id: string) => {
        delserv(
            id
        );
    }


    return (
        <Wrapper>
        {fetchStatus === FetchStatus.ERROR ?
            <Alert message={error}/>
        : null}
        {errorMessage ?
            <Alert message={message} />
        :null}
            <div className="services-panel">
                <div className="flex">
                    <h1 className="home__logo">Настройки дополнительных услуг</h1>
                    <div className="flex btn__right">
                        <button onClick={e => setShowModal(!showModal)} style={{width: '35px', height: '35px'}} className="btn mar auto__btn"><i className="fas fa-plus"></i></button>
                        <button onClick={e => saveAddServ()} className="btn btn__right create-cour__btn">Сохранить</button>
                    </div>
                    {showModal ?
                    <ServicesAdd onClick={e => setShowModal(!showModal)} />
                    :null}

                </div>
                {getAddServData?.data ? getAddServData?.data?.additionalServices.map(({id, price, enable, name}: any) => (
                <div onChange={e => {setIdenteficator(id); notification()}} style={{marginBottom: "20px"}} key={id} className="flex">
                <div className="flex-column services-panel__item">
                    <div className="home__title services-panel__title">Заголовок услуги в меню</div>
                    <input onChange={e => setName(e.target.value)} defaultValue={name} style={{width: '400px',height: '30px', boxSizing: 'border-box'}} type="text" className="ipt"/>
                </div>
                <div className="flex-column services-panel__item">
                    <div className="home__title services-panel__title">Стоимость услуги</div>
                    <input  onChange={e => setPrice(e.target.value)} defaultValue={price} style={{height: '30px', boxSizing: 'border-box'}} type="number" className="ipt"/>
                </div>
                <div className="flex-coumn">
                    <div className="home__title services-panel__title">Вкл</div>
                    <input onChange={e => setEnable(e.target.checked)} defaultChecked={enable} type="checkbox" className="checkbox"/>
                </div>
                <div className="flex">
                    <div onClick={e => delServ(id)} className="services-panel__close"><i className="fas fa-times"></i></div>
                </div>
            </div>
                )): "Нет дополнительных услуг!"}
            </div>
        </Wrapper>
    );
}
export const ServicesPanel = connect(
    ({ getaddservice }: State) => ({ ...getaddservice }),
    (dispatch) => {
        return {
            getaddserv: (data: any) => {
                return dispatch(getaddserv(data))
            },
            changeaddserv: (data: any) => {
                return dispatch(changeaddserv(data));
            },
            delserv: (data: any) => {
                return dispatch(delserv(data));
            }
        }
    }
)(ServicesPanelComponents)