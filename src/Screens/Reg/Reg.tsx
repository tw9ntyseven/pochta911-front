import React, { useCallback, useRef, useState } from 'react'
import './Reg.css'
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs'
import InputMask from "react-input-mask";
import { Auth } from '../../Components/Auth/Auth'
import { useForm } from 'react-hook-form';
import { Sms } from '../../Components/Sms/Sms';
import { Alert } from '../../Components/Alert/Alert';
import { connect } from 'react-redux';
import { reg } from '../../redux/actions/reg';
import { State } from '../../redux/redux';
import { RegistrationState } from '../../redux/types/reg';
import { FetchStatus } from '../../common/types';
import { history } from '../../redux/reducers';

interface RegState extends RegistrationState {
    reg: (data: any) => {};
}

export const RegComponent : React.FC<RegState> = ({ error, fetchStatus, reg, user }) => {
    // Forms
    const { register, errors, handleSubmit, watch } = useForm({});
    const pass = useRef({});
    pass.current = watch("password", "");
    const onSubmit = async (data: any) => {};

    // Inputs
    const [phone, setPhone] = useState('');
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    // bool
    const [showSms, setShowSms] = useState(false);
    const [hiddenValue, setHiddenValue] = useState(true);
    const [isReg, setIsReg] = useState();

    // Callbacks
    const phoneChangeCallback = useCallback(
        (e: any) => {
            setPhone(e.target.value.replace(/[^\d]/g, ''));
        }, []
    );
    const loginChangeCallback = useCallback(
        (e: any) => {
            setLogin(e.target.value);
        }, []
    );
    const passwordChangeCallback = useCallback(
        (e: any) => {
            setPassword(e.target.value);
        }, []
    );
    const toggleShow = useCallback( 
        (e: any) => {
            setHiddenValue(!hiddenValue)
        }, [hiddenValue]
    );

    const Submit = async(e: React.MouseEvent<HTMLElement>) => {  
        setShowSms(!showSms);
        reg({
            login,
            password,
            phone
        });
    }  

    return (
        <div className="wrapper">
        {fetchStatus === FetchStatus.ERROR ?
            <Alert message={error}/>
        : null}
            <h1 style={{textAlign: 'center'}} className="home__logo">Регистрация/Авторизация</h1>
            <div className="screen-margin reg">
                <Tabs className="reg__block">

                    <TabList className="reg__tablist">
                        <Tab className="reg__tab">Регистрация</Tab>
                        <Tab className="reg__tab">Вход</Tab>
                    </TabList>

                    <TabPanel className="reg__tabpanel">
                        
                        <div className="flex">
                        <span className="home__title">Логин</span>
                        {errors.login && <span className="btn__right errors-password">{errors.login.message}</span>}
                        </div>

                        <input 
                        name="login"
                        onClick={handleSubmit(onSubmit)}
                        onChange={loginChangeCallback} 
                        type="text" 
                        className="reg__ipt"
                        ref={register({
                            required: 'Это поле обязательно',
                            pattern: {
                                value: /^[a-zA-Z]+$/,
                                message: 'Некорректный формат логина'
                            }
                        })}
                        required
                        />

                        <div className="flex">
                        <span className="home__title">Пароль</span>
                        {errors.password && <span className="btn__right errors-password">{errors.password.message}</span>}
                        </div>

                        <div className="flex">
                        <input 
                        onChange={passwordChangeCallback} 
                        name="password"
                        type={hiddenValue ? "password" : "text"}
                        className="reg__ipt"
                        ref={register({
                            required: "Вы должны указать пароль",
                            pattern: {
                                value: /^[a-zA-Z0-9-]+$/,
                                message: 'Некорректный формат пароля'
                            },
                            minLength: {
                                value: 8,
                                message: "Пароль должен быть не менее 8 символов"
                            }
                        })}
                        required
                        />
                        <div onClick={toggleShow} className="reg__hide"><i className="fas fa-eye"></i></div>
                        </div>

                        <div className="flex">
                        <span className="home__title">Подтверждение пароля</span>
                        {errors.password_repeat && <p className="btn__right errors-password">{errors.password_repeat.message}</p>}
                        </div>

                        <input 
                        name="password_repeat"
                        type="password" 
                        className="reg__ipt"
                        ref={register({
                            validate: value =>
                                value === pass.current || "Пароли не совпадают"
                            })}
                            required
                        />
                        <span className="home__title">Телефон</span>
                        <InputMask 
                        mask="+7 (999) 999-99-99" 
                        value={phone} onChange={phoneChangeCallback} 
                        type="phone" 
                        autoComplete="phone" 
                        className="reg__ipt" 
                        placeholder="+7 (978) 000-00-00" 
                        required
                        />

                        {login !== '' && password !== '' && phone !== '' ?
                        <button onClick={e => {Submit(e); handleSubmit(onSubmit)}} className="btn reg__btn">Подтвердить телефон</button>
                        :<button disabled className="reg__btn disabled">Подтвердить телефон</button>}
                        {showSms && user ? 
                        <Sms userId={user.userId} onClick={e => setShowSms(!showSms)} />
                        : null}
                    </TabPanel>

                    <TabPanel className="reg__tabpanel">
                        <Auth />
                    </TabPanel>
                    </Tabs>
            </div>
        </div>
    );
}

export const Reg = connect(
    ({ registration }: State) => ({ ...registration }),
    (dispatch) => {
        return {
            reg: (data: any) => {
                return dispatch(reg(data))
            }
        }
    }
)(RegComponent)