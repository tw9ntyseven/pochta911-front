import React, { useState } from 'react'
import {Wrapper} from '../../../Components/Wrapper/Wrapper';
import DatePicker from 'react-datepicker'
import './NotWorking.css'
import { ButtonBack } from '../../../Components/ButtonBack/ButtonBack';

export const NotWorking : React.FC = () => {
    const [ dateValue, setDateValue ] = useState<any>(null);

    return (
        <Wrapper>
            <div className="flex">
                <ButtonBack />
                <h1 className="home__logo">Установка выходных</h1>
            </div>
            <div className="delivery-address__item not-working__inputs">
                <div style={{marginRight: '20px', marginBottom: 'auto', alignItems: 'center'}}
                    className="flex-column">
                    <div style={{marginBottom: '20px'}} className="home__title">Включить</div>
                    <input type="checkbox" className="checkbox"/>
                </div>
                <div style={{marginRight: '20px', marginBottom: 'auto', alignItems: 'center'}} className="flex-column">
                    <div style={{marginBottom: '20px'}} className="home__title">Сообщение клиентам</div>
                    <textarea style={{width: '500px'}} className="ipt notif__ipt"></textarea>
                </div>
                <div style={{marginRight: '20px', marginBottom: 'auto', alignItems: 'center'}} className="flex-column">
                    <div style={{marginBottom: '20px'}} className="home__title">Дата</div>
                    <DatePicker
                        locale="ru"
                        placeholderText="01.11.2020"
                        className="ipt export-excel__datepicker admin-panel__datepicker"
                        selected={dateValue}
                        onChange={date => setDateValue(date)}
                        dateFormat='dd.MM.yyyy'/>
                    <button style={{marginTop: '20px'}} className="btn">Добавить</button>
                </div>
                <div className="flex-column">
                    <div className="margin-bottom mar home__title">2020-01-01 <i style={{color: 'red'}} className="fas fa-trash-alt"></i></div>
                    <div className="margin-bottom mar home__title">2020-01-01 <i style={{color: 'red'}} className="fas fa-trash-alt"></i></div>
                    <div className="margin-bottom mar home__title">2020-01-01 <i style={{color: 'red'}} className="fas fa-trash-alt"></i></div>
                    <div className="margin-bottom mar home__title">2020-01-01 <i style={{color: 'red'}} className="fas fa-trash-alt"></i></div>
                    <div className="margin-bottom mar home__title">2020-01-01 <i style={{color: 'red'}} className="fas fa-trash-alt"></i></div>
                </div>
            </div>
        </Wrapper>
    );
}