import React, { useState } from 'react'
import './GroupList.css'
import { Wrapper } from '../../../Components/Wrapper/Wrapper'
import { Link } from 'react-router-dom'
import { IExportExcel } from '../../../Components/ExportExcel/ExportExcel'
import { ButtonBack } from '../../../Components/ButtonBack/ButtonBack'

type GroupListTableProps = {
    items: Array<any>,
}

const AddGroup: React.FC<IExportExcel> = ({onClick}) => {
    return (
        <div className="add-group__layout">
            <div className="add-group">
                <div className="flex export-excel__title-block">
                    <div className="home__title">Создать группу</div>
                    <div className="export-excel__btn" onClick={onClick}><i className="fas fa-times"></i></div>
                </div>
                <div className="flex add-group__margin">
                    <div className="home__title mar">Название:</div>
                    <input type="text" className="ipt"/>
                </div>
                <button style={{marginTop: '10px'}} className="btn">Создать</button>
            </div>
        </div>
    );
}

const EditGroup: React.FC<IExportExcel> = ({onClick}) => {
    return (
        <div className="add-group__layout">
            <div className="add-group">
                <div className="flex export-excel__title-block">
                    <div className="home__title">Редактирование группы</div>
                    <div className="export-excel__btn" onClick={onClick}><i className="fas fa-times"></i></div>
                </div>
                <div className="flex add-group__margin">
                    <div className="home__title mar">Название:</div>
                    <input type="text" defaultValue="hello" className="ipt"/>
                </div>
                <button style={{marginTop: '10px'}} className="btn">Сохранить</button>
            </div>
        </div>
    );
}

const GroupListTable = ({items}: GroupListTableProps) => {
    const [showEditGroup, setShowEditGroup] = useState<boolean>(false)
    return (
        <tbody>
            {items.map((item, index) => (
                <tr key={index}>
                    <td className="table-map__item bold">{item.id}</td>
                    <td className="table-map__item bold">{item.name}</td>
                    <td width="70" className="table-map__item"><Link style={{background: '#5bc0de'}} className="btn" to="/collaborator"><i className="fas fa-th-list"></i></Link></td>
                    <td width="70" className="table-map__item"><Link onClick={e => setShowEditGroup(!showEditGroup)} className="btn" to="#"><i className="fas fa-pencil-alt"></i></Link></td>
                    <td width="70" className="table-map__item"><Link style={{background: '#f0ad4e'}} className="btn" to="/admin/groups/groups-law"><i className="fas fa-cog"></i></Link></td>
                    <td width="70" className="table-map__item"><Link style={{background: '#d9534f'}} className="btn" to="#"><i className="fas fa-times-circle"></i></Link></td>
                </tr>
            ))}
            {showEditGroup ?
            <EditGroup onClick={e => setShowEditGroup(!showEditGroup)} />
            :null}
        </tbody>
    );
}

export const GroupList : React.FC = () => {
    const [showAddGroup, setShowAddGroup] = useState<boolean>(false)
    return (
        <Wrapper>
            <div className="group-list">
                <div className="flex">
                    <ButtonBack />
                    <h1 className="home__logo">Список групп</h1>
                    <button onClick={e => setShowAddGroup(!showAddGroup)} className="btn btn__right create-cour__btn"><i className="fas fa-user-friends mar"></i>Создать группу</button>
                    {showAddGroup ?
                    <AddGroup onClick={e => setShowAddGroup(!showAddGroup)} />
                    :null}
                </div>
                <table>
                    <thead>
                        <tr className="table__title-overlay">
                            <th className="table__title">ID</th>
                            <th className="table__title">Название</th>
                            <th className="table__title">Список пользователей</th>
                            <th className="table__title">Редактировать</th>
                            <th className="table__title">Права</th>
                            <th className="table__title">Удалить</th>
                        </tr>
                    </thead>
                    <GroupListTable items={[
                            {
                                id: '1',
                                name: 'Администраторы'
                            },
                            {
                                id: '2',
                                name: 'Клиенты'
                            },
                            {
                                id: '3',
                                name: 'Логисты'
                            },
                        ]} />
                </table>
            </div>
        </Wrapper>
    );
}