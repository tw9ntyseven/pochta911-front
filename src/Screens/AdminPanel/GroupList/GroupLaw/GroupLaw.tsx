import React, { useState } from 'react'
import {Link} from 'react-router-dom';
import {Wrapper} from '../../../../Components/Wrapper/Wrapper'

type GroupLawTableProps = {
    items: Array<any>,
    subitems: Array<any>,
    show: boolean
}

const GroupLawTable = ({items, subitems, show}: GroupLawTableProps) => {
    return (
        <tbody>
            {items.map((item, index) => (
                <>
                <tr key={index}>
                    <td width="70" className="table-map__item bold">{item.id}</td>
                    <td className="table-map__item bold">{item.name}</td>
                    <td width="100" className="table-map__item tar"><input type="checkbox" className="checkbox-20"/></td>
                </tr>
                {show ? 
                    <>
                    {subitems.map((item, index) => (
                    <tr className="table-map__item" key={index}>
                        <td className="table-map__item"></td>
                        <td className="table-map__item">
                            <span className="mar">{item.number}</span>
                            {item.name}
                        </td>
                        <td className="table-map__item flex tar">
                            <div className="flex mar"><i className="far fa-eye"></i>{item.access}</div>
                            <input type="checkbox" className="checkbox-20"/>
                        </td>
                    </tr>
                    ))}
                    </> : null}
                </>
            ))}

        </tbody>
    );
}

export const GroupLaw : React.FC = () => {
    const [showAddMode, setShowAddMode] = useState<boolean>(false);
    return (
        <Wrapper>
            <div className="group-law">
                <div className="flex">
                    <h1 className="home__logo">Настройка прав группы Администраторы</h1>
                    <div className="flex btn__right">
                        <div onClick={e => setShowAddMode(!showAddMode)} className="create-order__radio-item create-order__radio-item--active"><i className="fas fa-asterisk"></i></div>
                        <input type="checkbox" className="checkbox-20 mar"/>
                        <div className="home__title mar">Детальный режим</div>
                        <Link style={{height: '24px'}} to="/admin/groups" className="btn mar">
                            <i className="fas fa-user-friends mar"></i>Список групп</Link>
                        <button className="btn create-cour__btn">Сохранить</button>
                    </div>
                </div>
                <table className="table-auto">
                    <thead>
                        <tr className="table__title-overlay">
                            <th className="table__title">ID</th>
                            <th className="table__title">Название</th>
                            <th className="table__title">Наличие доступа</th>
                        </tr>
                    </thead>
                    <GroupLawTable items={[
                        {
                            id: '2',
                            name: 'Страницы',
                        },
                        {
                            id: '3',
                            name: 'Администрирование',
                        },
                        {
                            id: '5',
                            name: 'Главная страница',
                        },
                        {
                            id: '13',
                            name: 'Заказы',
                        },
                    ]} subitems={[
                        {
                            number: '452',
                            name: 'Операции с Действиями (del, add, toGroup, delFromGroup) (action)',
                            access: 'Группа'
                        },
                        {
                            number: '452',
                            name: 'Операции с Действиями (del, add, toGroup, delFromGroup) (action)',
                            access: 'Группа'
                        },
                    ]} show={showAddMode} />
                </table>
            </div>
        </Wrapper>
    );
}