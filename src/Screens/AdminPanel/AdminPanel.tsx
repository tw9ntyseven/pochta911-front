import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import './AdminPanel.css'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import { Wrapper } from '../../Components/Wrapper/Wrapper';

export const AdminPanel : React.FC = () => {
    const [ dateValue, setDateValue ] = useState<any>(null);
    const [ dateToValue, setDateToValue ] = useState<any>(null);
    return (
        <Wrapper>
                <div className="admin-panel">
                    <div className="flex">
                    <div className="flex-column">
                <h1 className="home__logo">Контент</h1>
                    <div className="flex">
                        <Link to="#" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-file"></i></div>
                            <div className="home__title">Страницы</div>
                        </Link>
                        <Link to="#" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="far fa-newspaper"></i></div>
                            <div className="home__title">Новости</div>
                        </Link>
                        <Link to="/clients/client" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-flag"></i></div>
                            <div className="home__title">Заказы</div>
                        </Link>
                        <Link to="/logistics" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-shipping-fast"></i></div>
                            <div className="home__title">Логист</div>
                        </Link>
                        <Link to="/notification" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-envelope-open-text"></i></div>
                            <div className="home__title">Оповещения</div>
                        </Link>
                        <Link to="/admin/get-cars-calc" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-money-bill-wave"></i></div>
                            <div className="home__title">Расчет с курьерами</div>
                        </Link>
                    </div>
                <h1 className="home__logo">Настройки</h1>
                <div className="flex">
                        <Link to="/admin/tg-journal" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fab fa-telegram"></i></div>
                            <div className="home__title">Telegram</div>
                        </Link>
                        <Link to="/admin/viber-journal" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fab fa-viber"></i></div>
                            <div className="home__title">Viber</div>
                        </Link>
                        <Link to="/clients" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-users"></i></div>
                            <div className="home__title">Клиенты</div>
                        </Link>
                        <Link to="/auto" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-car"></i></div>
                            <div className="home__title">Автоштат</div>
                        </Link>
                        <Link to="/admin/groups" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="fas fa-user-friends"></i></div>
                            <div className="home__title">Группы</div>
                        </Link>
                        <Link to="/admin/not-working" className="flex-column admin-panel__item">
                            <div className="admin-panel__ico"><i className="far fa-calendar-alt"></i></div>
                            <div className="home__title">Нерабочие даты</div>
                        </Link>
                    </div>
                    </div>
                        <div style={{marginBottom: 'auto'}}>
                            <h1 className="home__logo">Отчет</h1>
                            <div className="flex export-excel__inputs">
                            <DatePicker
                                locale="ru"
                                placeholderText="01.11.2020"
                                className="ipt export-excel__datepicker admin-panel__datepicker"
                                selected={dateValue}
                                onChange={date => setDateValue(date)}
                                dateFormat='dd.MM.yyyy'
                                />
                                <div className="home__title">до</div>
                                <DatePicker
                                locale="ru"
                                placeholderText="01.11.2020"
                                className="ipt admin-panel__datepicker"
                                selected={dateToValue}
                                onChange={date => setDateToValue(date)}
                                dateFormat='dd.MM.yyyy'
                                />
                                <button className="btn admin-panel__button">Выгрузить</button>
                            </div>
                        </div>
                    </div>
                </div>
          </Wrapper>
    );
}