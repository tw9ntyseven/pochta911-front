import React from 'react'
import { Wrapper } from '../../../Components/Wrapper/Wrapper';

type TgJournalTableProps = {
    items: Array<any>,
}

const TgJournalTable = ({items}: TgJournalTableProps) => {
    return (
        <tbody>
            {items.map((item, index) => (
                <tr key={index}>
                    <td className="table-map__item bold">{item.date}</td>
                    <td className="table-map__item bold">{item.userName}</td>
                    <td className="table-map__item">{item.idChat}</td>
                    <td className="table-map__item">{item.connectUser}</td>
                    <td width="700" className="table-map__item">{item.message}</td>
                    <td className="table-map__item">{item.answer}</td>
                </tr>
            ))}
        </tbody>
    );
}

export const TgJournal : React.FC = () => {
    return (
        <Wrapper>
            <div className="tg-journal">
                <h1 className="home__logo"><i className="fab fa-telegram mar"></i>Журнал Telegram</h1>
                <div className="flex">
                    <span className="home__title">Показать</span>
                        <select style={{width: 'auto'}} className="ipt mar create-cour__select">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span className="home__title">записей</span>
                        <div className="btn__right">
                            <span className="home__title">Поиск</span>
                            <input type="text" className="ipt"/>
                        </div>
                </div>
                <table className="table-auto mar-top">
                    <thead>
                        <tr className="table__title-overlay">
                            <th className="table__title">Дата</th>
                            <th className="table__title">Отправитель</th>
                            <th className="table__title">Id Чата</th>
                            <th className="table__title">Связанный пользователь</th>
                            <th className="table__title">Сообщение</th>
                            <th className="table__title">Ответ</th>
                        </tr>
                    </thead>
                    <TgJournalTable items={[
                        {
                            date: '2018-04-09 21:36:13',
                            userName: 'Дмитрий Щепкин',
                            idChat: '507999415',
                            connectUser: 'Дмитрий Щепкин',
                            message: 'Вы назначены на заказ Заказ № 111291 Дата заказа: 10.04.2018 Откуда: Выборгское шоссе, 17 к1 Адрес доставки: ул Коммунаров (Ольгино), д 44, - Комментарий: ДОСТАВКА К 9 УТРА! Период забора: 07:00 - 00:00 Период получения: 09:00 - 09:00 Получатель:',
                            answer: '/order_accepted_111472'
                        },
                        {
                            date: '2018-04-09 21:36:13',
                            userName: 'Дмитрий Щепкин',
                            idChat: '507999415',
                            connectUser: 'Дмитрий Щепкин',
                            message: 'Вы назначены на заказ Заказ № 111291 Дата заказа: 10.04.2018 Откуда: Выборгское шоссе, 17 к1 Адрес доставки: ул Коммунаров (Ольгино), д 44, - Комментарий: ДОСТАВКА К 9 УТРА! Период забора: 07:00 - 00:00 Период получения: 09:00 - 09:00 Получатель:',
                            answer: '/order_accepted_111472'
                        },
                    ]} />
                </table>
            </div>
        </Wrapper>
    );
}