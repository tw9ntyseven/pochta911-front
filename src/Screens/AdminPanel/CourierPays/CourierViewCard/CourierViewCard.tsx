import React, {useState} from 'react'
import {Link} from 'react-router-dom';
import './CourierViewCard.css'
import {ButtonBack} from '../../../../Components/ButtonBack/ButtonBack';
import {ExportExcel} from '../../../../Components/ExportExcel/ExportExcel';
import {Wrapper} from '../../../../Components/Wrapper/Wrapper';

type CourierViewCardTableProps = {
    items: Array<any>,
}

const CourierViewCardTable = ({items}: CourierViewCardTableProps) => {
    const [showExcel, setShowExcel] = useState(false);
    return (
        <tbody>
            {items.map((item, index) => (
                <tr key={index}>
                    <td className="table-map__item bold">{index + 1}</td>
                    <td className="table-map__item bold">{item.name}</td>
                    <td className="table-map__item">{item.balance}</td>
                    <td className="table-map__item">{item.last}</td>
                    <td className="table-map__item">
                    <button onClick={e => setShowExcel(!showExcel)} className="btn auto__btn">
                                <i className="fas fa-file-excel"></i>
                            </button>
                            {showExcel ?
                                <ExportExcel onClick={e => setShowExcel(!showExcel)} />
                            : null}
                    </td>
                    <td className="table-map__item">
                        {item.firstPrice}<br/><hr/>{item.secondPrice}
                    </td>
                    <td className="table-map__item">
                        {item.firstPrice}<br/><hr/>{item.secondPrice}
                    </td>
                    <td className="table-map__item">
                        {item.firstPrice}<br/><hr/>{item.secondPrice}
                    </td>
                </tr>
            ))}
        </tbody>
    );
}

export const CourierViewCard : React.FC = () => {
    const [showExcel, setShowExcel] = useState(false);
    return (
        <Wrapper>
            <div className="flex courier-view-card__header courier-view-card__height">
                <ButtonBack/>
                <input style={{width: '65px'}} defaultValue="2020" type="number" className="ipt courier-view-card__height"/>
                <div className="courier-view-card__button courier-view-card__height">
                    <i className="far fa-save"></i>
                </div>
                <Link to="/admin/get-cars-calc/courier/courier-view-card" className="btn courier-view-card__height">Отчет по наличным</Link>
                <button onClick={e => setShowExcel(!showExcel)} className="btn home__btn-excel">
                    <i className="fas fa-file-excel mar"></i>
                    Выгрузить в всё</button>
                {showExcel
                    ? <ExportExcel onClick={e => setShowExcel(!showExcel)}/>
                    : null}
            </div>

            <table className="table-auto">
                <thead>
                <tr className="table__title-overlay">
                    <th className="table__title">№</th>
                    <th className="table__title">Имя</th>
                    <th className="table__title">Баланс</th>
                    <th className="table__title">Прошлый</th>
                    <th className="table__title">Отчет</th>
                    <th className="table__title">1</th>
                    <th className="table__title">2</th>
                    <th className="table__title">3</th>
                </tr>
                </thead>

                <CourierViewCardTable items={[
                    {
                        name: "Нева-Салют",
                        balance: '11 288.38',
                        last: '0',
                        firstPrice: '5 888.28',
                        secondPrice: '0'
                    },
                    {
                        name: "Нева-Салют",
                        balance: '11 288.38',
                        last: '0',
                        firstPrice: '5 888.28',
                        secondPrice: '0'
                    },
                ]} />

            </table>

        </Wrapper>
    );
}