import React, { useState } from 'react'
import './CourierPays.css'
import { Wrapper } from '../../../Components/Wrapper/Wrapper';
import { Link } from 'react-router-dom';
import DatePicker, { registerLocale } from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import ru from 'date-fns/locale/ru';
import { ButtonBack } from '../../../Components/ButtonBack/ButtonBack';
registerLocale('ru', ru)

type CourierPaysTableProps = {
    items: Array<any>,
}

const CourierPaysTable = ({items}: CourierPaysTableProps) => {
    return (
        <tbody>
            {items.map((item, index) => (
                <tr key={index}>
                    <td className="table-map__item bold">{item.id}</td>
                    <td className="table-map__item bold">{item.userName}</td>
                    <td className="table-map__item">{item.balance}</td>
                    <td width="200" className="table-map__item"><Link to="/admin/get-cars-calc/courier" className="btn">Просмотр</Link></td>
                </tr>
            ))}
        </tbody>
    );
}

export const CourierPays : React.FC = () => {
    const [ dateValue, setDateValue ] = useState<any>(null);
    const [ dateToValue, setDateToValue ] = useState<any>(null);
    return (
        <Wrapper>
            <div className="courier-pays">
                <div className="flex">
                    <ButtonBack />
                    <h1 className="home__logo">Расчет с курьерами</h1>
                </div>
                <div className="flex courier-pays__buttons">
                    <Link to="/admin/get-cars-calc/courier/courier-view-card" className="btn mar">Отчет по курьерам</Link>
                    <Link to="/admin/get-cars-calc/courier/courier-view-card" className="btn mar">Отчет по клиентам наличка</Link>
                    <Link to="/admin/get-cars-calc/courier/courier-view-card" className="btn">Отчет по клиентам карты</Link>
                    <div className="flex btn__right">
                    <DatePicker
                        locale="ru"
                        placeholderText="01.11.2020"
                        className="ipt home__ipt-datepicker export-excel__datepicker"
                        selected={dateValue}
                        onChange={date => setDateValue(date)}
                        dateFormat='dd.MM.yyyy'
                        />
                        <div className="home__title">до</div>
                        <DatePicker
                        locale="ru"
                        placeholderText="01.11.2020"
                        className="ipt home__ipt-datepicker"
                        selected={dateToValue}
                        onChange={date => setDateToValue(date)}
                        dateFormat='dd.MM.yyyy'
                        />
                        <button className="btn">Обновить</button>
                    </div>
                </div>
                <div className="flex">
                    <span className="home__title">Показать</span>
                        <select style={{width: 'auto'}} className="ipt mar create-cour__select">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span className="home__title">записей</span>
                        <div className="btn__right">
                            <span className="home__title">Поиск</span>
                            <input type="text" className="ipt"/>
                        </div>
                </div>
                <table className="table-auto mar-top">
                    <thead>
                        <tr className="table__title-overlay">
                            <th className="table__title">ID</th>
                            <th className="table__title">ФИО</th>
                            <th className="table__title">Баланс</th>
                            <th className="table__title"></th>
                        </tr>
                    </thead>
                    <CourierPaysTable items={[
                        {
                            id: 1,
                            userName: 'Михайлов Антон',
                            balance: "0.00",
                        },
                        {
                            id: 2,
                            userName: 'Иванон Иван',
                            balance: 12.00,
                        },
                        {
                            id: 1,
                            userName: 'Михайлов Антон',
                            balance: 0.00,
                        },
                        {
                            id: 2,
                            userName: 'Иванон Иван',
                            balance: 12.00,
                        },
                    ]} />
                </table>
            </div>
        </Wrapper>
    );
}