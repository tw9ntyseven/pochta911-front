import React, { useState } from 'react'
import {ButtonBack} from '../../../../Components/ButtonBack/ButtonBack';
import {Wrapper} from '../../../../Components/Wrapper/Wrapper';
import DatePicker from 'react-datepicker'
import { Link } from 'react-router-dom';

type CourierTableProps = {
    items: Array<any>,
}

const CourierTable = ({items}: CourierTableProps) => {
    return (
        <tbody>
            {items.map((item, index) => (
                <tr key={index}>
                    <td className="table-map__item bold">{item.id}</td>
                    <td className="table-map__item bold">{item.date}</td>
                    <td className="table-map__item">{item.incassation}</td>
                    <td className="table-map__item">{item.courier}</td>
                    <td className="table-map__item">{item.back}</td>
                    <td className="table-map__item">
                        <input type="number" className="ipt"/>
                    </td>
                    <td className="table-map__item">
                        <label className="flex">
                            <input type="checkbox" className="checkbox-20"/>
                            <div className="home__title">Проверено</div>
                        </label>
                        <label className="flex">
                            <input type="checkbox" className="checkbox-20"/>
                            <div className="home__title">Подтвердить</div>
                        </label>
                        <label className="flex">
                            <input type="checkbox" className="checkbox-20"/>
                            <div className="home__title">Не сошлось</div>
                        </label>
                    </td>
                    <td width="100" className="table-map__item"><Link className="btn" to="#">Сохранить</Link></td>
                </tr>
            ))}
        </tbody>
    );
}

export const Courier : React.FC = () => {
    const [ dateValue, setDateValue ] = useState<any>(null);
    const [ dateToValue, setDateToValue ] = useState<any>(null);
    return (
        <Wrapper>
            <div className="flex">
                <ButtonBack/>
                <h1 className="home__logo">Коровин Александр</h1>
                <div className="btn__right flex">
                <div style={{marginRight: '20px'}} className="flex export-excel__inputs">
                    <DatePicker
                        locale="ru"
                        placeholderText="01.11.2020"
                        className="ipt export-excel__datepicker admin-panel__datepicker"
                        selected={dateValue}
                        onChange={date => setDateValue(date)}
                        dateFormat='dd.MM.yyyy'
                        />
                        <div className="home__title">до</div>
                        <DatePicker
                        locale="ru"
                        placeholderText="01.11.2020"
                        className="ipt admin-panel__datepicker"
                        selected={dateToValue}
                        onChange={date => setDateToValue(date)}
                        dateFormat='dd.MM.yyyy'
                        />
                        <button className="btn admin-panel__button">Обновить</button>
                            </div>   
                <h1 style={{fontSize: '24px'}} className="home__logo">Баланс:<br/>26 806 руб.</h1>
                </div>
            </div>
            <table className="table-auto mar-top">
                <thead>
                    <tr className="table__title-overlay">
                        <th className="table__title">id</th>
                        <th className="table__title">Дата</th>
                        <th className="table__title">Инкасация</th>
                        <th className="table__title">Курьер</th>
                        <th className="table__title">Вернуть</th>
                        <th className="table__title">Возвращено</th>
                        <th className="table__title"></th>
                        <th className="table__title"></th>
                    </tr>
                </thead>

                <CourierTable items={[
                    {
                        id: '268',
                        date: '2020-12-04',
                        incassation: '30 470.36',
                        courier: '3 543.00',
                        back: '2 560.36',
                    },{
                        id: '268',
                        date: '2020-12-04',
                        incassation: '30 470.36',
                        courier: '3 543.00',
                        back: '2 560.36',
                    },
                ]} />

            </table>
        </Wrapper>
    );
}