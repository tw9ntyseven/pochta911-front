import React from 'react'
import { CreateCourIpt } from '../../Components/CreateCourIpt/CreateCourIpt'
import { Wrapper } from '../../Components/Wrapper/Wrapper'
import './CreateCourier.css'

type CreateCourCardProps = {
    items: Array<any>,
}

export const CreateCourCard = ({items}: CreateCourCardProps) => (
    <div>
        {items.map((item, index) => (
            <div key={index} className="create-cour__card">
                <div className="create-cour__num">{index + 1}</div>
                <div style={{width: '100%'}} className="flex-column">
                    <input type="number" placeholder="Номер карты Сбербанка" className="ipt create-cour__ipt create-cour__card-ipt"/>
                    <textarea placeholder="Коментарии к номеру карты" className="ipt create-cour__ipt create-cour__card-ipt"></textarea>
                </div>
            </div>
        ))}
        <div style={{width: '100px',marginTop: '15px'}} className="flex btn__right">
            <button className="btn auto__btn create-order__btn"><i className="fas fa-plus"></i></button>
            <button style={{background: "#d9534f"}} className="btn auto__btn create-order__btn"><i className="fas fa-minus"></i></button>
        </div>
    </div>
)

export const CreateCourier : React.FC = () => {
    return (
        <Wrapper>
                <div className="create-cour">
                    <div className="flex">
                        <h1 className="home__logo">Курьер/автомобиль:</h1>
                        <button className="btn btn__right create-cour__btn">Сохранить</button>
                    </div>
                    <div className="flex">
                    <div className="create-cour__ipts">
                    <div className="home__title create-order__title create-cour__title">Контакты / Характеристики</div>
                        <CreateCourIpt disabled={false} title="ФИО:" type="text" />
                        <CreateCourIpt disabled={false} title="Телефон:" type="text" />
                        <CreateCourIpt disabled={false} title="Телефон (доп.):" type="text" />
                        <CreateCourIpt disabled={true} title="Email:" type="text" />
                        <CreateCourIpt disabled={true} title="Telegram:" type="text" />
                        <CreateCourIpt disabled={true} title="Viber:" type="text" />
                        <CreateCourIpt disabled={false} title="Марка:" type="text" />
                        <CreateCourIpt disabled={false} title="Номер:" type="text" />
                        <CreateCourIpt disabled={false} title="Год:" type="text" />
                        <CreateCourIpt disabled={false} title="Объем:" type="text" />
                        <div className="create-cour__flex">
                            <span className="home__title create-cour__subtitle">Тип авто:</span>
                            <select className="ipt btn__right create-cour__ipt create-cour__select" name="typeAuto" id="type">
                                <option value="">Седан</option>
                                <option value="">Хэтчбек</option>
                                <option value="">Лифтбек</option>
                                <option value="">Универсал</option>
                            </select>
                        </div>
                    </div>
                    <div className="create-cour__cards">
                    <div className="home__title create-order__title create-cour__title">Оплата</div>
                        <CreateCourCard items={[{},{}]} />
                    </div>
                    </div>
                </div>
         </Wrapper>
    );
}