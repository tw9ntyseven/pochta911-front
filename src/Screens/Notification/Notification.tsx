import React from 'react'
import './Notification.css'

export const Notification : React.FC = () => {
    return (
        <div className="wrapper">
            <div className="screen-margin notif">
                <div className="container">
                    <div className="flex">
                        <h1 className="home__logo">Оповещения</h1>
                        <div className="btn__right notif__message">
                            <i className="fas fa-exclamation mar"></i>
                            Отправка для всех клиентов</div>
                    </div>
                    <div className="flex">
                        <textarea className="ipt notif__ipt"></textarea>
                        <button className="btn notif__btn">
                            <i className="fas fa-paper-plane"></i>
                        </button>
                    </div>
                    <div className="flex notif__checkbox">
                        <span className="home__title">Уведомление по:</span>
                        <label style={{background: '#E6E2E2'}} className="home__statuses-item">
                            <input type="checkbox" className="home__statuses-item-checkbox"/>
                            Email
                        </label>
                        <label style={{background: '#E6E2E2'}} className="home__statuses-item">
                            <input type="checkbox" className="home__statuses-item-checkbox"/>
                            Всплывающее окно
                        </label>
                    </div>
                </div>
            </div>
        </div>
    );
}