import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { FetchStatus } from '../../common/types';
import { Alert } from '../../Components/Alert/Alert';
import { Wrapper } from '../../Components/Wrapper/Wrapper';
import { changepriceforcar, getpriceforcar } from '../../redux/actions/getpriceforcar';
import { State } from '../../redux/redux';
import { GetPriceForCarState } from '../../redux/types/getpriceforcar';
import './PricePanel.css'


export interface IPricePanel extends GetPriceForCarState {
    getpriceforcar: (data: any) => {};
}

interface PricePanelProps extends IPricePanel {
    changepriceforcar: (data: any) => {};
}

const PricePanelItem = ({data, add, deleteItem, save}: any) => {
    const [from, setFrom] = useState('');
    const [to, setTo] = useState('');
    const [price, setPrice] = useState('');

    const [errorMessage, setErrorMessage] = useState(false);
    const [message, setMessage] = useState('');
    

    const pushProps = () => {
        if (from === "" || to === "" || price === ""){
            setErrorMessage(true);
            setMessage("Заполните все поля!");
        } else {
        let fromNum = Number(from);
        let toNum = Number(to);
        let priceNum = Number(price);

        add.push({
            from: fromNum,
            to: toNum,
            price: priceNum
        })
        save();
    }

        // setErrorMessage(true);
        // setMessage("Добавлено, для сохранения нажмите СОХРАНИТЬ");
 
    }
    const deleteProps = (id: string) => {
        // setErrorMessage(true);
        // setMessage("Удалено, для сохранения нажмите СОХРАНИТЬ");
        deleteItem.push({
            id
        })
        save();
    }

    return (
        <tbody className="price-panel__items">
        {errorMessage ?
            <Alert message={message} />
        :null}
                <tr className="price-panel__item price-panel__additem">
                    <td className="price-panel__center"><input onChange={e => setFrom(e.target.value)} style={{width: '90%'}} type="number" className="ipt"/></td>
                    <td className="price-panel__center"><input onChange={e => setTo(e.target.value)} style={{width: '90%'}} type="number" className="ipt"/></td>
                    <td className="price-panel__center"><input onChange={e => setPrice(e.target.value)} style={{width: '90%'}} type="number" className="ipt"/></td>
                    <td onClick={e => pushProps()} style={{color: 'green', cursor: "pointer"}} className="price-panel__center"><i className="fas fa-plus-circle"></i></td>
                </tr>
            {data?.data?.pricePerKilometer.map(({id, from, to, price}: any) => (
                <tr key={id} className="price-panel__item">
                    <td className="price-panel__center"><input onChange={e => setFrom(e.target.value)} defaultValue={from} style={{width: '90%'}} type="number" className="ipt"/></td>
                    <td className="price-panel__center"><input onChange={e => setTo(e.target.value)} defaultValue={to} style={{width: '90%'}} type="number" className="ipt"/></td>
                    <td className="price-panel__center"><input onChange={e => setPrice(e.target.value)} defaultValue={price} style={{width: '90%'}} type="number" className="ipt"/></td>
                    <td onClick={e => deleteProps(id)} style={{color: 'red', cursor: "pointer"}} className="price-panel__center"><i className="fas fa-trash"></i></td>
                </tr>
            ))}
        </tbody>
    );
}

export const PricePanelComponent : React.FC<PricePanelProps> = ({error, fetchStatus, getpriceforcar, priceForCarData, changepriceforcar}) => {
    const [crossingNeva, setCrossingNeva] = useState('');
    const [increaseProcent, setIncreaseProcent] = useState('');
    const [exactTimePrice, setExactTimePrice] = useState('');
    const [outOfCAD, setOutOfCAD] = useState('');
    const [exactTimeEnable, setExactTimeEnable] = useState<boolean>();
    const [payBankCardCommissionEnable, setPayBankCardCommissionEnable] = useState<boolean>();
    const [outOfCity, setOutOfCity] = useState('');
    const [payBankCardCommissionProcent, setPayBankCardCommissionProcent] = useState('');
    const [suburbIncreaseKilometers, setSuburbIncreaseKilometers] = useState('');
    const [suburbIncreaseProcent, setSuburbIncreaseProcent] = useState('');
    const [vsevolozhsk, setVsevolozhsk] = useState('');
    const [pricePerKilometerAdd, setPricePerKilometerAdd] = useState([]);
    const [pricePerKilometerDel, setPricePerKilometerDel] = useState([]);

    const [errorMessage, setErrorMessage] = useState(false);
    const [message, setMessage] = useState('');

    useEffect(() => {
        getpriceforcar({});
    }, [])

    const getValues = () => {
        let crossingNevaNum = Number(crossingNeva);
        let increaseProcentNum = Number(increaseProcent);
        let exactTimePriceNum = Number(exactTimePrice);
        let outOfCADNum = Number(outOfCAD);
        let outOfCityNum = Number(outOfCity);
        let payBankCardCommissionProcentNum = Number(payBankCardCommissionProcent);
        let suburbIncreaseKilometersNum = Number(suburbIncreaseKilometers);
        let suburbIncreaseProcentNum = Number(suburbIncreaseProcent);
        let vsevolozhskNum = Number(vsevolozhsk);

        let obj: any = {}
        if (crossingNevaNum !== 0) obj.crossingNeva = crossingNevaNum;
        if (increaseProcentNum !== 0) obj.increaseProcent = increaseProcentNum;
        if (exactTimePriceNum !== 0) obj.exactTimePrice = exactTimePriceNum;
        if (outOfCADNum !== 0) obj.outOfCAD = outOfCADNum;
        if (outOfCityNum !== 0) obj.outOfCity = outOfCityNum;
        if (payBankCardCommissionProcentNum !== 0) obj.payBankCardCommissionProcent = payBankCardCommissionProcentNum;
        if (exactTimeEnable !== null) obj.exactTimeEnable = exactTimeEnable;
        if (payBankCardCommissionEnable !== null) obj.payBankCardCommissionEnable = payBankCardCommissionEnable;
        if (suburbIncreaseKilometersNum !== 0) obj.suburbIncreaseKilometers = suburbIncreaseKilometersNum;
        if (suburbIncreaseProcentNum !== 0) obj.suburbIncreaseProcent = suburbIncreaseProcentNum;
        if (vsevolozhskNum !== 0) obj.vsevolozhsk = vsevolozhskNum;
        if (pricePerKilometerAdd.length !== 0) obj.pricePerKilometerAdd = pricePerKilometerAdd;
        if (pricePerKilometerDel.length !== 0) obj.pricePerKilometerDel = pricePerKilometerDel;
        
        return obj;
    }
    const savePricePanel = () => {
        setErrorMessage(true);
        setMessage("Данные сохранены");
        changepriceforcar({
            ...getValues()
        })
    }

    return (
            <Wrapper>
        {fetchStatus === FetchStatus.ERROR ?
                <Alert message={error}/>
        : null}
        {errorMessage ?
            <Alert message={message} />
        :null}
                <div className="price-panel">
                <div className="flex">
                <h1 className="home__logo">Стоимость за километр</h1>
                <button onClick={e => savePricePanel()} className="btn btn__right create-cour__btn">Сохранить</button>
                </div>
                    <div className="flex-column price-panel__inputs">
                        <div className="flex margin-bottom price-panel__overlay">
                            <div className="home__title price-panel__title">Стоимость пересечения Невы</div>
                            <input onChange={e => setCrossingNeva(e.target.value)} type="number" defaultValue={priceForCarData?.data?.crossingNeva} placeholder="0" className="btn__right ipt price-panel__ipt"/>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">За пределами Санкт-Петербурга</div>
                            <input onChange={e => setOutOfCity(e.target.value)} type="number" defaultValue={priceForCarData?.data?.outOfCity} placeholder="22" className="btn__right ipt price-panel__ipt"/>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">За попадание в Геозону за КАДом</div>
                            <input onChange={e => setOutOfCAD(e.target.value)} type="number" defaultValue={priceForCarData?.data?.outOfCAD} placeholder="79" className="btn__right ipt price-panel__ipt"/>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">Стоимость во Всеволожск (вместо за КАДом)</div>
                            <input onChange={e => setVsevolozhsk(e.target.value)} type="number" defaultValue={priceForCarData?.data?.vsevolozhsk} placeholder="280" className="btn__right ipt price-panel__ipt"/>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">Прибавление к стоимости за "к точному времени"</div>
                            <input onChange={e => setExactTimePrice(e.target.value)} type="number" defaultValue={priceForCarData?.data?.exactTimePrice} placeholder="100" className="btn__right ipt price-panel__ipt"/>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">Прибавление к стоимости (%) за пробег с выездом в пригород</div>
                            <div className="flex btn__right">
                            <div className="home__title">Пробег в км</div>
                            <input onChange={e => setSuburbIncreaseKilometers(e.target.value)} defaultValue={priceForCarData?.data?.suburbIncreaseKilometers} style={{height: '27px', paddingLeft: '5px', marginRight: '10px'}} type="number" placeholder="25" className="ipt"/>
                            <div className="home__title">Надбавка в %</div>
                            <input onChange={e => setSuburbIncreaseProcent(e.target.value)} defaultValue={priceForCarData?.data?.suburbIncreaseProcent} style={{height: '27px'}} type="number" placeholder="15" className="ipt"/>
                            </div>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">Безусловная надбавка к стоимости (%)</div>
                        <div className="flex btn__right">
                        <div className="home__title">Надбавка в %</div>
                            <input onChange={e => setIncreaseProcent(e.target.value)} defaultValue={priceForCarData?.data?.increaseProcent} style={{height: '27px', paddingLeft: '5px'}} type="number" placeholder="0" className="ipt"/>
                        </div>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">Опция "К точному времени" включена?</div>
                            <div className="flex btn__right">
                                <div className="home__title">Вкл</div>
                                <input onChange={e => setExactTimeEnable(e.target.checked)} defaultChecked={priceForCarData?.data?.exactTimeEnable} style={{width: '30px', height: '30px'}} type="checkbox" className="ipt"/>
                            </div>
                        </div>
                        <div className="flex margin-bottom price-panel__overlay">
                        <div className="home__title price-panel__title">Банковская комиссия за оплату по картам:</div>
                        <div className="flex btn__right">
                        <div className="home__title">% от инкассации</div>
                            <input onChange={e => setPayBankCardCommissionProcent(e.target.value)} defaultValue={priceForCarData?.data?.payBankCardCommissionProcent} style={{height: '27px', paddingLeft: '5px', marginRight: '10px'}} type="number" placeholder="1.7" className="btn__right ipt"/>
                        <div className="home__title">Вкл</div>
                            <input onChange={e => setPayBankCardCommissionEnable(e.target.checked)} defaultChecked={priceForCarData?.data?.payBankCardCommissionEnable} style={{width: '30px', height: '30px'}} type="checkbox" className="btn__right ipt"/>
                        </div>
                        </div>
                    </div>
                    <table style={{paddingBottom: "100px"}} className="table-auto">
                        <thead>
                            <tr className="price-panel__table-overlay">
                                <th>От</th>
                                <th>До</th>
                                <th>Стоимость</th>
                                <th></th>
                            </tr>
                        </thead>
                        <PricePanelItem save={() => savePricePanel()} add={pricePerKilometerAdd} deleteItem={pricePerKilometerDel} data={priceForCarData} />
                    </table>
                </div>
          </Wrapper>
    );
}
export const PricePanel = connect(
    ({ getpriceforcars }: State) => ({ ...getpriceforcars }),
    (dispatch) => {
        return {
            getpriceforcar: (data: any) => {
                return dispatch(getpriceforcar(data))
            },
            changepriceforcar: (data: any) => {
                return dispatch(changepriceforcar(data))
            }
        }
    }
)(PricePanelComponent)