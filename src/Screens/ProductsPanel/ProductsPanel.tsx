import React from 'react'
import './ProductsPanel.css'
import {Wrapper} from '../../Components/Wrapper/Wrapper'
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

type ProductsPanelItemProps = {
    title: string
}

const PPSelect = () => {
    return (
        <select name="" id="" className="ipt products-panel__ipt product-panel__select">
            <option value="Больше">Больше</option>
            <option value="Меньше">Меньше</option>
            <option value="Равно">Равно</option>
        </select>
    );
}

const ProductsPanelItem = ({title} : ProductsPanelItemProps) => {
    return (
        <tr className="products-panel__item">
            <td className="products-panel__title price-panel__center">{title}</td>
            <td className="products-panel__title price-panel__center"><PPSelect/></td>
            <td className="products-panel__title price-panel__center"><input placeholder="0" className="ipt products-panel__ipt" type="number"/></td>
            <td className="products-panel__title price-panel__center"><input placeholder="30" className="ipt products-panel__ipt" type="number"/></td>
            <td className="products-panel__title price-panel__center"><input className="checkbox" type="checkbox"/></td>
            <td className="products-panel__title price-panel__center"><input placeholder="1" className="ipt products-panel__ipt" type="number"/></td>
        </tr>
    );
}

export const ProductsPanel : React.FC = () => {
    return (
        <Wrapper>
            <Tabs className="tabs">

                <TabList className="flex tablist">
                    <Tab className="reg__tab">Настройка условий</Tab>
                    <Tab className="reg__tab">Список типов товаров</Tab>
                </TabList>

                <TabPanel className="tabpanel">
                    <div className="flex">
                        <h1 className="home__logo">Настройка стоимости за перевозку товаров</h1>
                        <button className="btn btn__right create-cour__btn">Сохранить условия</button>
                    </div>
                    <table className="table-auto">
                        <thead>
                            <tr className="products-panel__overlay">
                                <th className="products-panel__title">Товар</th>
                                <th className="products-panel__title">Условие</th>
                                <th className="products-panel__title">Количество</th>
                                <th className="products-panel__title">Стоимость</th>
                                <th className="products-panel__title">Фиксированная</th>
                                <th className="products-panel__title">Множитель</th>
                            </tr>
                        </thead>
                        <tbody>
                            <ProductsPanelItem title="Сверток/Пакет до 5кг"/>
                            <ProductsPanelItem title="Коробка/Пакет до 10кг"/>
                            <ProductsPanelItem title="Коробка/Пакет до 15кг"/>
                            <ProductsPanelItem title="Коробка/Пакет до 20кг"/>
                            <ProductsPanelItem title="Коробка/Пакет до 25кг"/>
                            <ProductsPanelItem title="Коробка/Пакет до 30кг"/>
                            <ProductsPanelItem title="Цветы"/>
                            <ProductsPanelItem title="Торт"/>
                            <ProductsPanelItem title="Продукты питания"/>
                        </tbody>
                    </table>
                </TabPanel>
                <TabPanel className="tabpanel">
                    <div className="flex">
                        <h1 className="home__logo">Названия товаров</h1>
                        <button className="btn btn__right create-cour__btn">Сохранить названия</button>
                    </div>
                    <table className="products-panel__table">
                        <thead>
                            <tr className="products-panel__overlay">
                                <th className="products-panel__title">Id</th>
                                <th className="products-panel__title">Название</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>1</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>2</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>3</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>4</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>5</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>6</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                            <tr className="products-panel__item products-panel__title price-panel__center">
                                <td>7</td>
                                <td><input defaultValue="Сверток/Пакет до 5кг" type="text" className="ipt products-panel__ipt-name"/></td>
                            </tr>
                        </tbody>
                    </table>
                </TabPanel>

            </Tabs>
        </Wrapper>
    );
}