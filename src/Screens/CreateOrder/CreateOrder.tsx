import React, { useState } from 'react'
import './CreateOrder.css'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import { ItemDeliveryAddress } from '../../Components/ItemDeliveryAddress/ItemDeliveryAddress';
import { Clock } from '../../Components/Clock/Clock';
import moment from 'moment';
import { ItemDeliveryTime } from '../../Components/ItemDeliveryTime/ItemDeliveryTime';
import { ButtonBack } from '../../Components/ButtonBack/ButtonBack';

function handleClick(e: React.MouseEvent<HTMLElement>) {
    var foo = document.querySelectorAll(".create-order__radio-item");

    for (let i = 0; i < foo.length; i++) {
      foo[i].classList.remove("create-order__radio-item--active");
    }
    e.currentTarget.classList.add("create-order__radio-item--active");
};

export const CreateOrder : React.FC = () => {
    const [carRadio, setCarRadio] = useState<boolean>(true);
    const [peopleRadio, setPeopleRadio] = useState<boolean>(false);
    const [heel, setHeel] = useState<boolean>(false);
    const [dateValue, setDateValue] = useState<any>(null);
    const [showEditBlock, setShowEditBlock] = useState<boolean>(false);

    return (
        <div className="wrapper">
            <div className="screen-margin create-order">
                <div className="container">
                    <div className="create-order__block">
                        <div className="create-order__time-block">
                            <ButtonBack />
                            <input style={{width: '300px'}} list="users" placeholder="проба()" type="text" className="create-order__ipt"/>
                            <datalist id="users">
                                <option value="Праздник Маркет">Праздник Маркет</option>
                                <option value="Проект Хлеб">Проект Хлеб</option>
                                <option value="Рога и копыта">Рога и копыта</option>
                            </datalist>
                            <Clock />
                        </div>
                        <div className="flex create-order__order-info">
                        <div className="create-order__radio">
                            <div onClick={e => {setPeopleRadio(false); setHeel(false); setCarRadio(!carRadio); handleClick(e)}} className="create-order__radio-item create-order__radio-item--active"><i className="fas fa-car"></i></div>
                            {carRadio ? <div className="create-order__radio-item-text">На автомобиле</div>: null}
                            <div onClick={e => {setPeopleRadio(!peopleRadio); setHeel(false); setCarRadio(false); handleClick(e)}} className="create-order__radio-item"><i className="fas fa-walking"></i></div>
                            {peopleRadio ? <div className="create-order__radio-item-text">Пешая доставка</div>: null}
                            <div onClick={e => {setPeopleRadio(false); setHeel(!heel); setCarRadio(false); handleClick(e)}} className="create-order__radio-item"><i className="fas fa-truck"></i></div>
                            {heel ? <div className="create-order__radio-item-text">Каблук</div>: null}
                        </div>
                        <span className="home__title">Дата заказа:</span>
                            <DatePicker
                            locale="ru"
                            className="ipt home__ipt-datepicker create-order__datepicker"
                            placeholderText={moment(new Date()).format("DD.MM.YYYY")}
                            selected={dateValue}
                            onChange={date => setDateValue(date)}
                            dateFormat='dd.MM.yyyy'
                            />
                            <div style={{marginRight: "10px"}} className="home__title">Откуда:</div>
                            <div style={{textDecoration: "underline"}} className="home__title">г. Санкт-Петербург</div>
                            {/* <select id="selectAddress" className="ipt create-order__ipt-select">
                                <option key="index" value="г. Санкт-Петербург">г. Санкт-Петербург</option>
                                <option key="index1" value="В ручную">Ручной ввод</option>
                            </select> */}
                            <div onClick={e => setShowEditBlock(!showEditBlock)} className="btn btn__right create-order__btn-edit"><i className="fas fa-pencil-alt"></i></div>
                        </div>

                        {/* EDIT BLOCK */}
                        <div className={`create-order__edit-block ${showEditBlock ? "create-order__edit-block" : "create-order__edit-block--hide"}`}>
                        <div className="flex">
                            <div style={{width: '50%', marginRight: '10px'}} className="mar flex-column">
                                <span className="home__title">Адрес отправления</span>
                                <input disabled={showEditBlock ? false : true} placeholder="г. Санкт-Петербург" type="text" className="edit-block__ipt"/>
                            </div>
                            <div style={{width: '16.6%', marginRight: '10px'}} className="flex-column">
                                <span className="home__title">Подъезд</span>
                                <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                            </div>
                            <div style={{width: '16.6%', marginRight: '10px'}} className="flex-column">
                                <span className="home__title">Этаж</span>
                                <input disabled={showEditBlock ? false : true} type="number" className="edit-block__ipt"/>
                            </div>
                            <div style={{width: '16.6%'}} className="flex-column">
                                <span className="home__title">кв/офис/помещ</span>
                                <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                            </div>
                        </div>
                        <div className="flex">
                        <div style={{width: '50%', marginRight: '10px'}} className="flex-column">
                            <span className="home__title">Отправитель ФИО</span>
                            <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                        </div>
                        <div style={{width: '50%'}} className="flex-column">
                            <span className="home__title">Телефон отправителя</span>
                            <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                        </div>
                        </div>

                        <ItemDeliveryTime disabled={showEditBlock ? false : true} inTime={false} />

                        <div style={{marginTop: '20px'}} className="flex">
                        <div style={{width: '33%', marginRight: '10px'}} className="flex-column">
                            <span className="home__title">Инкассация</span>
                            <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                        </div>
                        <div style={{width: '33%', marginRight: '10px'}} className="flex-column">
                            <span className="home__title">Цена доставки</span>
                            <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                        </div>
                        <div style={{width: '33%'}} className="flex-column">
                            <span className="home__title">Оплата доставки</span>
                            <input disabled={showEditBlock ? false : true} type="text" className="edit-block__ipt"/>
                        </div>
                        </div>
                            <textarea style={{marginTop: '20px'}} disabled={showEditBlock ? false : true} placeholder="Примечания к адресу: Опишите особенности входа на арес и то какую задачу там необходимо выполнить ?  Например подойти на ресепшен и сказать что за документами для ООО Ромашка" className="edit-block__ipt edit-block__ipt--textarea"/>
                        </div>

                        <div className="flex create-order__margin">
                            <div className="home__title create-order__title">Адреса доставки:</div>
                            <div className="flex btn__right">
                                {/* <div onClick={e => setActive(!active)} className={`create-order__radio-item ${active ? 'create-order__radio-item--active' : 'create-order__radio-item'}`}><i className="fas fa-route"></i></div>
                                <div className="home__title">Оптимальный маршрут</div> */}
                                <button className="btn auto__btn create-order__btn"><i className="fas fa-plus"></i></button>
                            </div>
                        </div>
                        <ItemDeliveryAddress items={[{}]} />
                        
                        <div className="create-order__price">
                            <div style={{fontSize: '17px'}} className="home__title">Итоговая стоимость</div>
                            <h1 className="home__logo">1000 руб.</h1>
                        </div>
                        <div style={{paddingBottom: '50px'}} className="create-order__buttons">
                            <ButtonBack />
                            <div className="btn mar">Сохранить заказ</div>
                            {/* <div style={{background: '#f0ad4e'}} className="btn mar">Возврат на первый адрес</div> */}
                            <div style={{background: '#5bc0de'}} className="btn">Расчитать маршрут</div>
                        </div>
                    </div>
                    {/* <div className="create-order__map-block"></div> */}
                </div>
            </div>
        </div>
    );
}