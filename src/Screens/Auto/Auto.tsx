import React from 'react'
import {Link} from 'react-router-dom';
import { TableAuto } from '../../Components/TableAuto/TableAuto';

export const Auto : React.FC = () => {
    return (
        <div className="wrapper">
            <div className="screen-margin auto">
                <div className="container">
                    <div className="flex">
                        <h1 className="home__logo">Список курьеров/машин</h1>
                        <Link to="/auto/create-courier" className="btn btn__right">Добавить курьера</Link>
                    </div>
                    <div className="flex">
                    <span className="home__title">Показать</span>
                        <select style={{width: 'auto'}} className="ipt mar create-cour__select">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span className="home__title">записей</span>
                        <div className="btn__right">
                        <span className="home__title">Поиск</span>
                        <input type="text" className="ipt"/>
                        </div>
                    </div>
                    <div className="table">
                        <TableAuto />
                    </div>
                </div>
            </div>
        </div>
    );
}