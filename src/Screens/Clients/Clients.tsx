import React, { useState } from 'react'
import {Link} from 'react-router-dom';
import { ExportExcel } from '../../Components/ExportExcel/ExportExcel';
import { TableCli } from '../../Components/TableCli/TableCli';

export const Clients : React.FC = () => {
    const [showExcel, setShowExcel] = useState<boolean>(false)



    return (
        <div className="wrapper">
            <div className="screen-margin auto">
                <div className="container">
                    <div className="flex">
                        <h1 className="home__logo">Список клиентов</h1>
                        <div className="flex btn__right">
                        <Link to="/clients/client" className="btn"><i className="fas fa-user-plus mar"></i> Новый клиент</Link>
                            <button onClick={e => setShowExcel(!showExcel)} className="btn auto__btn">
                                <i className="fas fa-file-excel"></i>
                            </button>
                            {showExcel ?
                                <ExportExcel onClick={e => setShowExcel(!showExcel)} />
                            : null}
                        </div>
                    </div>
                    <div className="flex">
                    <span className="home__title">Показать</span>
                        <select style={{width: 'auto'}} className="ipt mar create-cour__select">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span className="home__title">записей</span>
                        <div className="btn__right">
                        <span className="home__title">Поиск</span>
                        <input type="text" className="ipt"/>
                        </div>
                    </div>
                    <div className="table">
                        <TableCli />
                    </div>
                </div>
            </div>
        </div>
    );
}