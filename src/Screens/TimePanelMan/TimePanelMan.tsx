import React, { useEffect, useState } from 'react'
import {Wrapper} from '../../Components/Wrapper/Wrapper';
import { connect } from 'react-redux';
import { State } from '../../redux/redux';
import { FetchStatus } from '../../common/types';
import { Alert } from '../../Components/Alert/Alert';
import { changetimeforman, gettimeforman } from '../../redux/actions/gettimeforman';
import { GetTimeForManState } from '../../redux/types/gettimeforman';

export interface ITimePanelMan extends GetTimeForManState {
    gettimeforman: (data: any) => {};
}

interface TimePanelManProps extends ITimePanelMan {
    changetimeforman: (data: any) => {};
}

export const TimePanelManComponent : React.FC<TimePanelManProps> = ({error, fetchStatus, gettimeforman, changetimeforman, timeForManData}) => {
    const [errorMessage, setErrorMessage] = useState(false);
    const [message, setMessage] = useState('');

    const [deadlineDeliveryMin, setDeadlineDeliveryMin] = useState('');
    const [deliveryPeriodForAllMin, setDeliveryPeriodForAllMin] = useState('');
    const [orderReadinessDuring, setOrderReadinessDuring] = useState('');
    const [orderReadinessFrom, setOrderReadinessFrom] = useState('');
    const [orderReadinessTo, setOrderReadinessTo] = useState('');
    const [orderTodayDelivery, setOrderTodayDelivery] = useState('');
    const [orderTodayLeave, setOrderTodayLeave] = useState('');
    const [orderTomorrowDelivery, setOrderTomorrowDelivery] = useState('');
    const [orderTomorrowLeave, setOrderTomorrowLeave] = useState('');
    const [samplingInterval, setSamplingInterval] = useState('');
    const [standardDeliveryPeriod, setStandardDeliveryPeriod] = useState('');
    const [standardDeliveryPeriodFromStart, setStandardDeliveryPeriodFromStart] = useState('');
    
    useEffect(() => {
        gettimeforman({});
    }, [])

    const getValues = () => {
        let deadlineDeliveryMinNum = Number(deadlineDeliveryMin);
        let deliveryPeriodForAllMinNum = Number(deliveryPeriodForAllMin);
        let orderReadinessDuringNum = Number(orderReadinessDuring);
        let orderReadinessFromNum = Number(orderReadinessFrom);
        let orderReadinessToNum = Number(orderReadinessTo);
        let orderTodayDeliveryNum = Number(orderTodayDelivery);
        let orderTodayLeaveNum = Number(orderTodayLeave);
        let orderTomorrowDeliveryNum = Number(orderTomorrowDelivery);
        let orderTomorrowLeaveNum = Number(orderTomorrowLeave);
        let samplingIntervalNum = Number(samplingInterval);
        let standardDeliveryPeriodNum = Number(standardDeliveryPeriod);
        let standardDeliveryPeriodFromStartNum = Number(standardDeliveryPeriodFromStart);

        let obj: any = {}
        if(deadlineDeliveryMinNum !== 0) obj.deadlineDeliveryMin = deadlineDeliveryMinNum;
        if(deliveryPeriodForAllMinNum !== 0) obj.deliveryPeriodForAllMin = deliveryPeriodForAllMinNum;
        if(orderReadinessDuringNum !== 0) obj.orderReadinessDuring = orderReadinessDuringNum;
        if(orderReadinessFromNum !== 0) obj.orderReadinessFrom = orderReadinessFromNum;
        if(orderReadinessToNum !== 0) obj.orderReadinessTo = orderReadinessToNum;
        if(orderTodayDeliveryNum !== 0) obj.orderTodayDelivery = orderTodayDeliveryNum;
        if(orderTodayLeaveNum !== 0) obj.orderTodayLeave = orderTodayLeaveNum;
        if(orderTomorrowDeliveryNum !== 0) obj.orderTomorrowDelivery = orderTomorrowDeliveryNum;
        if(orderTomorrowLeaveNum !== 0) obj.orderTomorrowLeave = orderTomorrowLeaveNum;
        if(samplingIntervalNum !== 0) obj.samplingInterval = samplingIntervalNum;
        if(standardDeliveryPeriodNum !== 0) obj.standardDeliveryPeriod = standardDeliveryPeriodNum;
        if(standardDeliveryPeriodFromStartNum !== 0) obj.standardDeliveryPeriodFromStart = standardDeliveryPeriodFromStartNum;

        return obj;
    }

    const saveTime = () => {
        setErrorMessage(true);
        setMessage("Данные сохранены");
        changetimeforman({
            ...getValues()
        });
    }

    return (
        <Wrapper>
        {fetchStatus === FetchStatus.ERROR ?
                 <Alert message={error}/>
         : null}
        {errorMessage ?
            <Alert message={message} />
        :null}
            <div className="time-panel">
                <div className="flex">
                    <h1 className="home__logo">Ограничения по времени</h1>
                    <button onClick={e => saveTime()} className="btn btn__right create-cour__btn">Сохранить</button>
                </div>
                <div className="flex-column time-panel__inputs">
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="mar">Заказ на <span className="home__title">завтра</span>с доставкой до</div>
                        <input min="0" max="24" onChange={e => setOrderTomorrowDelivery(e.target.value)} defaultValue={timeForManData?.data?.orderTomorrowDelivery} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч. можно оставлять до</div>
                        <input min="0" max="24" onChange={e => setOrderTomorrowLeave(e.target.value)} defaultValue={timeForManData?.data?.orderTomorrowLeave} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч.</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="mar">Заказ на <span className="home__title">сегодня</span>с доставкой до</div>
                        <input min="0" max="24" onChange={e => setOrderTodayDelivery(e.target.value)} defaultValue={timeForManData?.data?.orderTodayDelivery} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч. можно оставлять до</div>
                        <input min="0" max="24" onChange={e => setOrderTodayLeave(e.target.value)} defaultValue={timeForManData?.data?.orderTodayLeave} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч.</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="mar">Заказ с готовностью с</div>
                        <input min="0" max="24" onChange={e => setOrderReadinessDuring(e.target.value)} defaultValue={timeForManData?.data?.orderReadinessDuring} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">до</div>
                        <input min="0" max="24" onChange={e => setOrderReadinessFrom(e.target.value)} defaultValue={timeForManData?.data?.orderReadinessFrom} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">можно доставить в течении</div>
                        <input min="0" max="24" onChange={e => setOrderReadinessTo(e.target.value)} defaultValue={timeForManData?.data?.orderReadinessTo} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч.</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="mar">Стандартный период доставки</div>
                        <input min="0" max="24" onChange={e => setStandardDeliveryPeriod(e.target.value)} defaultValue={timeForManData?.data?.standardDeliveryPeriod} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч.</div>
                        <div className="mar">от времени готовности.</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="mar">Стандартный период <span className="home__title">начала</span> доставки</div>
                        <input min="0" max="24" onChange={e => setStandardDeliveryPeriodFromStart(e.target.value)} defaultValue={timeForManData?.data?.standardDeliveryPeriodFromStart} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч.</div>
                        <div className="mar">от времени готовности <span className="home__title">"по"</span>.</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="">Крайнее время доставки <span className="home__title">не может быть меньше</span></div>
                        <input min="0" max="24" onChange={e => setDeadlineDeliveryMin(e.target.value)} defaultValue={timeForManData?.data?.deadlineDeliveryMin} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">ч.</div>
                        <div className="mar">от времени заказа.</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="">Период доставки всех заказов <span className="home__title">не менее</span></div>
                        <input min="0" max="59" onChange={e => setDeliveryPeriodForAllMin(e.target.value)} defaultValue={timeForManData?.data?.deliveryPeriodForAllMin} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">минут.</div>
                        <div className="mar">(Кроме заказов к точному времени)</div>
                    </div>
                    <div className="flex price-panel__overlay time-panel__overlay">
                        <div className="">Интервал забора <span className="home__title">не менее</span></div>
                        <input min="0" max="59" onChange={e => setSamplingInterval(e.target.value)} defaultValue={timeForManData?.data?.samplingInterval} type="number" className="ipt time-panel__ipt"/>
                        <div className="mar">минут.</div>
                    </div>
                </div>
            </div>
        </Wrapper>
    );
}
export const TimePanelMan = connect(
    ({ gettimeformans }: State) => ({ ...gettimeformans }),
    (dispatch) => {
        return {
            gettimeforman: (data: any) => {
                return dispatch(gettimeforman(data))
            },
            changetimeforman: (data: any) => {
                return dispatch(changetimeforman(data))
            }
        }
    }
)(TimePanelManComponent)